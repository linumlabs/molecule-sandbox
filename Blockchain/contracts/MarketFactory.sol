pragma solidity ^0.5.0;

import "./openzeppelin-solidity/token/ERC721/IERC721.sol";
import "./markets/IMarketFactory.sol";
import "./escrowTypes/IEscrow.sol";

contract MarketFactory{
    address private owner;
    address private nftRegistry;
    address private reserveToken;

    uint256 internal publishedBlock_;

    mapping(uint256 => address) internal registeredFactories;
    mapping(uint256 => address[]) internal deployedMarkets;
    mapping(address => uint256) internal factoriesToIndex;

    uint256 internal index = 0;

    event MarketDeployed(
        address indexed _owner,
        address indexed _market,
        uint256 indexed _tokenId,
        uint256 _marketType
    );
    
    modifier onlyNFTFactory() {
        require(msg.sender == nftRegistry, "Only NFT factory can execute");
        _;
    }

    modifier onlyFactory(uint256 _factoryType) {
        require(registeredFactories[_factoryType] != address(0), "Invalid Factory");
        _;
    }
    
    modifier onlyValidNFT(uint256 _nftID) {
        require(IERC721(nftRegistry).ownerOf(_nftID) != address(0), "Invalid Factory");
        _;
    }
    
    constructor (address _nftRegistry, address _reserveToken) public {
        nftRegistry = _nftRegistry;
        owner = msg.sender;
        reserveToken = _reserveToken;
        publishedBlock_ = block.number;
    }

    function initialiseEscrow(address _escrow, address _targetMarket) 
        internal
    {
        IEscrow(_escrow).updateEscrow(_targetMarket);
    }

    function registerNewFactory(address _factory) 
        public
    {
        require(msg.sender == owner, "User not authorised to register");
        registeredFactories[index] = _factory;
        factoriesToIndex[_factory] = index;
        index = index + 1;
    }

    function findFactory(uint256 _index)
        public
        view
        returns(address)
    {
        return registeredFactories[_index];
    }

    function deployMarket(
        uint256 _marketType,
        uint256 _nftTokenID,
        uint256 _initialReserve,
        uint256 _marketCap,
        address _escrowAddress,
        uint256 _gradient
    )
        public
        onlyFactory(_marketType)
        onlyValidNFT(_nftTokenID)
    {
        address newMarket = IMarketFactory(
            registeredFactories[_marketType]
            ).deployMarket(
                _nftTokenID,
                _initialReserve,
                _marketCap,
                msg.sender,
                _escrowAddress,
                _gradient
            );
        deployedMarkets[_marketType].push(newMarket);
        initialiseEscrow(_escrowAddress, newMarket);

        emit MarketDeployed(msg.sender, newMarket, _nftTokenID, _marketType);
    }

    function getFactoryIndex(address _factory)
        public
        view
        returns(uint256)
    {
        return factoriesToIndex[_factory];
    }

    function getMarkets(uint256 _marketType) public view returns(address[] memory) {
        return deployedMarkets[_marketType];
    }
    
    /**
      * @return The block number the contract was published on.
      */
    function publishBlock() public view returns(uint256) {
        return publishedBlock_;
    }
}