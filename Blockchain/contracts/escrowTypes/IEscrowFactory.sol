pragma solidity ^0.5.0;

contract IEscrowFactory {
    address internal rootEscrow;

    event EscrowDeployed(address escrow);

    modifier onlyRootEscrow() {
        require(
            msg.sender == rootEscrow,
            "Invalid caller"
        );
        _;
    }

    function deployEscrow(address _nftOwner, address _marketFactory) external returns(address);

}