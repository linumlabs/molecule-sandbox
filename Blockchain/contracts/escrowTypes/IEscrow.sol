pragma solidity ^0.5.0;

contract IEscrow {
    address internal marketFactory_;
    bool internal marketRegistered_;
    address internal targetMarket_;

    event Deposited(address indexed payee, uint256 weiAmount);
    event Withdrawn(address indexed payee, uint256 weiAmount);
    
    modifier onlyMarketFactory(address _marketFactory) {
        require(
            marketFactory_ == _marketFactory,
            "Functionality only accesible by market factory"
        );
        _;
    }

    modifier notRegistered() {
        require(!marketRegistered_, "Market registered");
        _;
    }

    function withdraw() external returns(bool);

    function updateEscrow(address _targetMarket) external;
}