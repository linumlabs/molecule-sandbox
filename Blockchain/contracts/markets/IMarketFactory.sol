pragma solidity ^0.5.0;

contract IMarketFactory {

    address internal rootFactory;
    address internal nftRegistry;
    address internal reserveToken;

    modifier onlyRootFactory() {
        require(msg.sender == rootFactory, "Invalid caller");
        _;
    }

    function deployMarket(
        uint256 _nftTokenID,
        uint256 _initialReserve,
        uint256 _marketCap,
        address _nftOwner,
        address _escrowAddress,
        uint256 _gradient
    )
        external
        returns(address);

}