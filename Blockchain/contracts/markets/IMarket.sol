pragma solidity ^0.5.0;


interface IMarket /*is IERC20 */ {
    event Transfer(
      address indexed from,
      address indexed to,
      uint256 value
    );
    event Transfer(
      address indexed from, 
      address indexed to, 
      uint value, 
      bytes data
    );
    event Minted(uint256 amount, uint256 totalCost);
    event Burned(uint256 amount, uint256 reward);

    function totalSupply() external view returns (uint256);

    function marketCap() external view returns (uint256);

    function nftTokenID() external view returns (uint256);

    function balanceOf(address who) external view returns (uint256);

    function transfer(address to, uint256 value) external returns (bool);

    function getEscrow() external view returns(address);

    function priceToMint(uint256 numTokens) external view returns(uint256);
    
    function rewardForBurn(uint256 numTokens) external view returns(uint256);
    
    function mint(uint256 numTokens) external;
    
    function burn(uint256 numTokens) external;

    
}