pragma solidity ^0.5.0;

import "./escrowTypes/IEscrowFactory.sol";
import "./markets/IMarket.sol"; 
import "./openzeppelin-solidity/token/ERC721/IERC721.sol";

contract EscrowFactory {
    address private owner;
    uint256 private publishedBlock_;
    address private marketFactory_;
    address private nftRegistry_;
    uint256 internal index = 0;

    mapping(address => address[]) private _ownersEscrows;
    mapping(uint256 => address) internal registeredFactories;
    mapping(uint256 => address[]) internal deployedEscrows;

    event EscrowDeployed(
        address indexed _nftOwner,
        address indexed _newEscrow,
        address _escrowType
    );

    event EscrowFactoryRegistered(
        address indexed _factory,
        uint256 indexed _index
    );

    modifier onlyNFTOwner(address _owner) {
        require(
            IERC721(nftRegistry_).balanceOf(_owner) != 0,
            "Owner dose not have an NFT"
        );
        _;
    }

    modifier onlyFactory(uint256 _escrowType) {
        require(registeredFactories[_escrowType] != address(0), "Invalid Factory");
        _;
    }

    constructor (address _marketFactory, address _nftRegistry) public {
        owner = msg.sender;
        marketFactory_ = _marketFactory;
        nftRegistry_ = _nftRegistry;
        publishedBlock_ = block.number;
    }

    /**
      * @dev Registers a factory with the escrow factory. 
      * @notice A factory has to be registered in order to 
      *     be used. 
      * @param _factory : The address of the factory 
      */
    function registerNewFactory(address _factory) 
        public
    {
        require(msg.sender == owner, "User not authorised to register");
        registeredFactories[index] = _factory;
        emit EscrowFactoryRegistered(_factory, index);
        index = index + 1;
    }

    /**
      * @dev Ability to find factoryies by their index. 
      * @return address : The address of the factory
      */
    function findFactory(uint256 _index)
        public
        view
        returns(address)
    {
        return registeredFactories[_index];
    }

    /**
      * @dev Deployes an escrow. The escrow will own 
      *     the NFT owners shares of the market 
      *     untill the market is complete.
      * @param _escrowType : The index of the registered
      *     escrow factory.
      * @param _nftOwner : The address of the owner
      */
    function deployEscrow(
        uint256 _escrowType,
        address _nftOwner
    )
        public
        onlyFactory(_escrowType)
        onlyNFTOwner(_nftOwner)
    {
        address newEscrow = IEscrowFactory(
            registeredFactories[_escrowType]
        ).deployEscrow(
            _nftOwner,
            marketFactory_
        );
        deployedEscrows[_escrowType].push(newEscrow);

        emit EscrowDeployed(
            _nftOwner, 
            newEscrow, 
            registeredFactories[_escrowType]
        );
    }

    /**
      * @return The deployed escrows for the escrow type (index of factory)
      */
    function getEscrows(uint256 _escrowType) public view returns(address[] memory) {
        return deployedEscrows[_escrowType];
    }

    /**
      * @return The block number the contract was published on.
      */
    function publishBlock() public view returns(uint256) {
        return publishedBlock_;
    }
}