const etherlime = require('etherlime');
const ethers = require('ethers');
require('dotenv').config();

const INFURA_API_KEY = process.env.INFURA_API_KEY;

const RINKEBY_PRIVATE_KEY = process.env.RINKEBY_PRIVATE_KEY;
const DEVNET_PRIVATE_KEY = process.env.DEVNET_PRIVATE_KEY;
const PRIVATE_KEY = RINKEBY_PRIVATE_KEY ? RINKEBY_PRIVATE_KEY : DEVNET_PRIVATE_KEY;

const NFTRegistry = require('../build/NFTRegistry.json');
const PseudoDaiToken = require('../build/PseudoDaiToken.json');
const MarketFactory = require('../build/MarketFactory.json');
const BasicLinearFactory = require('../build/BasicLinearFactory.json');
const EscrowFactory = require('../build/EscrowFactory.json');
const BasicEscrowFactory = require('../build/BasicEscrowFactory.json');

const NFTRegistrySettings ={
    name: "Molecule Patent Registry",
    symbol: "MPR",
    eventSig: {
        transfer: 'Transfer(address,address,uint256)'
    }
}

const EscrowFactorySettings = {
	eventSig : {
		escrowDeployed: 'EscrowDeployed(address,address,address)'
	}
}

const marketFactorySettings ={
	eventSig: {
		marketDeployed:"MarketDeployed(address,address,uint256,uint256)"
	}
}

const marketSettings = {
	eventSig : {
		transfer: 'Transfer(address,address,uint256)'
	}
}

const defaultConfigs = {
    gasPrice: 20000000000,
    gasLimit: 4700000
}

const testData = [
 	{
		marketAddress: null,
		tokenId: null,
		moleculeName: 'Dihydrogen monoxide',
		ownedBy: null,
		escrowAddress: null,
		marketCap: "10000000000000000000000000000000000000",
		initialReserve: "9000000000000000000"
	},
	{
		marketAddress: null,
		tokenId: null,
		moleculeName: 'paracetamol acetaminophen',
		ownedBy: null,
		escrowAddress: null,
		marketCap: "10000000000000000000000000000000000000000",
		initialReserve: "9000000000000000000000000000"
	},
	{
		marketAddress: null,
		tokenId: null,
		moleculeName: 'ibuprofen',
		ownedBy: null,
		escrowAddress: null,
		marketCap: "10000000000000000000000000000000000000000000",
		initialReserve: "9000000000000000000000000000000",
	}
]

const deploy = async (network, secret) => {
	let deployer;

	if(!secret){
		secret = PRIVATE_KEY;
	}

	if(network){
		deployer = new etherlime.InfuraPrivateKeyDeployer(secret, network, INFURA_API_KEY, defaultConfigs);
	} else {
		deployer = new etherlime.EtherlimeDevnetDeployer(secret, 8545);
	}

	const pseudoDaiInstance = await deployer.deploy(PseudoDaiToken, false, "Pseudo Dai", "PDAI", 18);

	const nftInstance = await deployer.deploy(NFTRegistry, false, "Molecule", "MOL");
	
	const marketFactoryInstance = await deployer.deploy(MarketFactory, false, nftInstance.contract.address, pseudoDaiInstance.contract.address);
	const basicLinearFactoryInstance = await deployer.deploy(BasicLinearFactory, false, marketFactoryInstance.contract.address, nftInstance.contract.address, pseudoDaiInstance.contract.address);

	let deployerMarketFactoryInstance = marketFactoryInstance.contract.connect(deployer.wallet);
	await (await deployerMarketFactoryInstance.registerNewFactory(basicLinearFactoryInstance.contract.address)).wait()

	const escrowFactoryInstance = await deployer.deploy(EscrowFactory, false, marketFactoryInstance.contract.address, nftInstance.contract.address);
	const basicEscrowFactoryInstance = await deployer.deploy(BasicEscrowFactory, false, escrowFactoryInstance.contract.address);
	
	let deployerEscrowFactoryInstance = escrowFactoryInstance.contract.connect(deployer.wallet);
	await (await deployerEscrowFactoryInstance.registerNewFactory(basicEscrowFactoryInstance.contract.address)).wait()

	const CONTRACT_ADDRESSES = `
	PDAI_CONTRACT_ADDRESS=${pseudoDaiInstance.contract.address}
	NFTREGISTRY_CONTRACT_ADDRESS=${nftInstance.contract.address}
	MARKETFACTORY_CONTRACT_ADDRESS=${marketFactoryInstance.contract.address}
	ESCROWFACTORY_CONTRACT_ADDRESS=${escrowFactoryInstance.contract.address}
	APPLICATION_WALLET_PRIVATE_KEY=${PRIVATE_KEY}`;
	console.log(CONTRACT_ADDRESSES);

	if(network){
		// let deployerNFTRegistryInstance = await etherlime.ContractAt(NFTRegistry, '0x98614Be4dbC1a74DA6905f529476a96F07Fb9416', deployer.wallet, deployer.wallet.provider);
		// deployerEscrowFactoryInstance = await etherlime.ContractAt(EscrowFactory, '0xaD0ABbD1Dcd656e9089d4DC5B44d8de8568b6b81', deployer.wallet, deployer.wallet.provider);
		// deployerMarketFactoryInstance = await etherlime.ContractAt(MarketFactory, '0xbb3c38EFaEB13AEBF61b1f00408a5C0164c9A1eC', deployer.wallet, deployer.wallet.provider);

		let deployerNFTRegistryInstance = nftInstance.contract.connect(deployer.wallet);
		// console.log('Initialising test data');
		// for	(let i = 0; i < testData.length; i++){
		// 	let receipt = await (await deployerNFTRegistryInstance.publishMolecule(testData[i].moleculeName)).wait();
		// 	testData[i].tokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;
		// 	console.log("Token created");
			
		// 	let escrowReceipt = await (await deployerEscrowFactoryInstance.deployEscrow(0, deployer.wallet.address)).wait();
		// 	testData[i].escrowAddress = (escrowReceipt.events.filter(event => event.eventSignature == EscrowFactorySettings.eventSig.escrowDeployed))[0].args._newEscrow;
		// 	console.log("Escrow created");
			
		// 	let marketReceipt = await (await deployerMarketFactoryInstance.deployMarket(0, testData[i].tokenId, ethers.utils.bigNumberify(testData[i].initialReserve), ethers.utils.bigNumberify(testData[i].marketCap), testData[i].escrowAddress)).wait();
		// 	testData[i].marketAddress = (marketReceipt.events.filter(event => event.eventSignature == marketFactorySettings.eventSig.marketDeployed))[0].args._market;
		// 	console.log("Market created");

		// 	await (await deployerNFTRegistryInstance.safeTransferFrom(deployer.wallet.address, testData[i].marketAddress, testData[i].tokenId)).wait();
		// 	console.log("NFT transferred");
		// }
		// console.log(testData);
	}
	
};

module.exports = {
	deploy
};