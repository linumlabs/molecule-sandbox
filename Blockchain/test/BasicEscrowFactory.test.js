var MarketFactory = require('../build/MarketFactory.json');
var PseudoDaiToken = require('../build/PseudoDaiToken.json');
var NFTRegistry = require('../build/NFTRegistry.json');
var EscrowFactory = require('../build/EscrowFactory.json');
var BasicEscrowFactory = require('../build/BasicEscrowFactory.json');
var Escrow = require('../build/Escrow.json');

const etherlime = require('etherlime');

const nftRegistrySettings = {
    name: "The coolest tokens",
    symbol: "TCT",
    eventSig: {
        transfer: 'Transfer(address,address,uint256)'
    }
}

const moleculeData = {
    name: "caffeine",
    marketData: {
        tokenID: undefined,
        initialReserve: "1000",
        marketCap: "100000.0",
        escrowAddress: undefined
    }
}

const pseudoDaiSettings = {
    name: "PseudoDai",
    symbol: "pDAI",
    decimals: 18
}

describe('Basic Escrow Factory test', () => {
    let deployer;
    let adminAccount = devnetAccounts[1];
    let userAccount = devnetAccounts[2];

    let PseudoDaiInstance,
        NFTRegistryInstance,
        MarketFactoryInstance;

    /**
     * Sets up the needed contracts for testing.
     *     PDAI
     *     NFTRegistry 
     *     MarketFactory
     *     EscrowFactory
     *     BasicEscrowFactory
     * The basic factory is then linked up to 
     *     the root factory. 
     * An NFT is minted to the userAccount
     */
    beforeEach('', async () => {
        deployer = new etherlime.EtherlimeDevnetDeployer(adminAccount.secretKey);
        PseudoDaiInstance = await deployer.deploy(
            PseudoDaiToken,
            false,
            pseudoDaiSettings.name,
            pseudoDaiSettings.symbol,
            pseudoDaiSettings.decimals
        );
        NFTRegistryInstance = await deployer.deploy(
            NFTRegistry,
            false,
            nftRegistrySettings.name,
            nftRegistrySettings.symbol
        );
        MarketFactoryInstance = await deployer.deploy(
            MarketFactory,
            false,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );
        EscrowFactoryInstance = await deployer.deploy(
            EscrowFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address
        );
        BasicEscrowFactoryInstance = await deployer.deploy(
            BasicEscrowFactory,
            false,
            EscrowFactoryInstance.contract.address
        );
        await EscrowFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicEscrowFactoryInstance.contract.address);
        await NFTRegistryInstance
            .from(userAccount.wallet)
            .publishMolecule(moleculeData.name);
    });

    describe('Deploying an escrow', () => {
        /**
         * Deploys an escrow and checks that the address
         * is stored correctly in the contract
         */
        it("Deploying Escrow", async () => {
            let recipt = await (await EscrowFactoryInstance
                .from(userAccount.wallet)
                .deployEscrow(
                    0,
                    userAccount.wallet.address
                )).wait();
            let escrowAddress = await EscrowFactoryInstance
                .from(userAccount.wallet)
                .getEscrows(0);
            EscrowInstance = await etherlime.ContractAtDevnet(Escrow, escrowAddress[0]);
            assert.equal(
                recipt.events[0].args._nftOwner,
                userAccount.wallet.address,
                "Owner is incorrect"
            );
            assert.equal(
                recipt.events[0].args._newEscrow,
                EscrowInstance.contract.address,
                "Escrow address is incorrect"
            );
        });

        /**
         * Ensures that the modifiers are working and that 
         * a user cannot create a Escrow from the Basic Escrow 
         * Factory (instead they must use the Escrow Factory)
         */
        it("Testing modifiers", async () => {
            try {
                await BasicEscrowFactoryInstance
                    .from(userAccount.wallet)
                    .deployEscrow(
                        0,
                        userAccount.wallet.address
                    );
                assert.equal(true, false, "Able to create Escrow from Basic Escrow Factory");
            } catch (error) {
                assert.equal(true, true, "");
            }
        });
    });
});