var MarketFactory = require('../build/MarketFactory.json');
var BasicLinearFactory = require('../build/BasicLinearFactory.json');
var BasicLinearMarket = require('../build/BasicLinearMarket.json');
var PseudoDaiToken = require('../build/PseudoDaiToken.json');
var NFTRegistry = require('../build/NFTRegistry.json');
var EscrowFactory = require('../build/EscrowFactory.json');
var BasicEscrowFactory = require('../build/BasicEscrowFactory.json');
var Escrow = require('../build/Escrow.json');

const etherlime = require('etherlime');
const ethers = require('ethers');

const nftFactorySettings = {
    name: "The coolest tokens",
    symbol: "TCT",
    eventSig: {
        transfer: 'Transfer(address,address,uint256)'
    }
};

const marketFactorySettings = {
    eventSig: {
        MarketDeployed: "MarketDeployed(address,address,uint256,address)"
    }
};

const moleculeData = {
    name: "caffeine"
};

const marketData = {
    nftTokenID: 0,
    initialReserve: "10.0",
    marketCap: "1000",
    marketAddress: undefined
};

const pseudoDaiSettings = {
    name: "PseudoDai",
    symbol: "pDAI",
    decimals: 18
};

describe('Basic Linear Market test', async () => {
    let deployer;
    let adminAccount = devnetAccounts[1];
    let userAccount = devnetAccounts[2];

    let PseudoDaiInstance,
        NFTRegistryInstance,
        MarketFactoryInstance,
        BasicLinearFactoryInstance,
        basicLinearMarketInstance,
        EscrowFactoryInstance,
        BasicEscrowFactoryInstance,
        EscrowInstance;

    /**
     * Sets up the needed contracts for testing.
     *     PDAI
     *     NFTRegistry 
     *     MarketFactory
     *     BasicLinearFactory
     *     EscrowFactory
     *     BasicEscrowFactory
     * The basic factories are then linked up to 
     * their root factories. 
     * An NFT is then minted to the userAccount
     * UserAccount is also given 1000 PDAI
     * Creates a market. The userAccount then sends the 
     * NFT to the market to initialize the market and escrow
     */
    beforeEach('', async () => {
        deployer = await new etherlime.EtherlimeDevnetDeployer(adminAccount.secretKey);
        PseudoDaiInstance = await deployer.deploy(
            PseudoDaiToken,
            false,
            pseudoDaiSettings.name,
            pseudoDaiSettings.symbol,
            pseudoDaiSettings.decimals
        );
        
        NFTRegistryInstance = await deployer.deploy(
            NFTRegistry,
            false,
            nftFactorySettings.name,
            nftFactorySettings.symbol
        );

        MarketFactoryInstance = await deployer.deploy(
            MarketFactory,
            false,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );

        BasicLinearFactoryInstance = await deployer.deploy(
            BasicLinearFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );

        EscrowFactoryInstance = await deployer.deploy(
            EscrowFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address
        );

        BasicEscrowFactoryInstance = await deployer.deploy(
            BasicEscrowFactory,
            false,
            EscrowFactoryInstance.contract.address
        );

        await MarketFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicLinearFactoryInstance.contract.address);

        await EscrowFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicEscrowFactoryInstance.contract.address);

        //Creating NFT
        let reciept = await (await NFTRegistryInstance
            .from(userAccount.wallet)
            .publishMolecule(moleculeData.name)
        ).wait();

        marketData.nftTokenID = await (reciept.events.filter(
            event => event.eventSignature === nftFactorySettings.eventSig.transfer))[0].args.tokenId;

        let balance = await NFTRegistryInstance
            .from(userAccount.wallet)
            .balanceOf(userAccount.wallet.address);

        assert.strictEqual(balance.toNumber(), 1, "The user has their NFT");

        //userAccount given PDAI
        await PseudoDaiInstance.from(userAccount.wallet).mint();

        let clientPseudoDaiBalance = await PseudoDaiInstance
            .from(userAccount.wallet)
            .balanceOf(userAccount.wallet.address);

        assert.strictEqual(
            clientPseudoDaiBalance.toString(),
            ethers.utils.parseUnits("1000", pseudoDaiSettings.decimals).toString()
        );

        //Market & escrow created. NFT send to it
        await EscrowFactoryInstance
            .from(userAccount.wallet)
            .deployEscrow(
                0,
                userAccount.wallet.address
            );

        let escrowAddress = await EscrowFactoryInstance
            .from(userAccount.wallet)
            .getEscrows(
                0
            );

        EscrowInstance = await etherlime.ContractAtDevnet(Escrow, escrowAddress[0]);
        
        await MarketFactoryInstance
            .from(userAccount.wallet)
            .deployMarket(
                0,
                marketData.nftTokenID,
                (ethers.utils.parseUnits(marketData.initialReserve, 18)),
                (ethers.utils.parseUnits(marketData.marketCap, 18)),
                escrowAddress[0]
            );

        let marketAddress = await MarketFactoryInstance
            .from(userAccount.wallet)
            .getMarkets(0);

        basicLinearMarketInstance = await etherlime.ContractAtDevnet(
            BasicLinearMarket,
            marketAddress[0]
        );

        await NFTRegistryInstance
            .from(userAccount.wallet)
            .safeTransferFrom(
                userAccount.wallet.address,
                basicLinearMarketInstance.contract.address,
                marketData.nftTokenID
            );

        //the market is now set up and the owner of the NFT
        let escrowBalance = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(escrowAddress[0]);

        assert.equal(
            ethers.utils.formatUnits(escrowBalance, 18),
            marketData.initialReserve,
            "Escrow has not been given initial reserve"
        );
    });

    describe("market functions", () => {
        /**
         * Gets the price to mint the market cap,
         * approves the contract (in the PDAI) 
         * to be able to spend that amount and then
         * mints that amount.
         * The test then checks that the escrow 
         * will send the user their initial reserve 
         * once the market cap is reached. 
         * Tests users cannot mint past market cap
         */
        it("Minting", async () => {
            let escrowBalance = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(EscrowInstance.contract.address);
            assert.equal(
                ethers.utils.formatUnits(escrowBalance, 18),
                '10.0',
                "Escrow balance is not initial reserve"
            );
            let costToMint = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .priceToMint(ethers.utils.parseUnits("1000", 18));
            assert.strictEqual(
                ethers.utils.formatUnits(costToMint, 18).toString(),
                "250.0",
                "Invalid costToMint() result"
            );
            
            let balanceOfEscrow = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(EscrowInstance.contract.address);
            let totalSupply = await basicLinearMarketInstance.from(userAccount.wallet).totalSupply();
            
            assert.strictEqual(
                ethers.utils.formatUnits(balanceOfEscrow, 18).toString(),
                "10.0",
                "Escrow dose not own initial reserve"
            );
            assert.strictEqual(
                ethers.utils.formatUnits(totalSupply, 18).toString(),
                "10.0",
                "Escrow dose not own initial reserve"
            );

            let priceToMint = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .priceToMint(ethers.utils.parseUnits("990", 18));
            await PseudoDaiInstance
                .from(userAccount.wallet)
                .approve(
                    basicLinearMarketInstance.contract.address,
                    priceToMint
                );
            await basicLinearMarketInstance
                .from(userAccount.wallet)
                .mint(ethers.utils.parseUnits("989", 18));
            let totalSupplyAfterMint = await basicLinearMarketInstance.from(userAccount.wallet).totalSupply();
            try {
                await EscrowInstance.from(userAccount.wallet).withdraw();
                assert.equal(true, false, "Escrow did not prevent premature withdraw");
            } catch (error) {
                assert.equal(true, true, "Escrow did prevent premature withdraw");
            }
            assert.notEqual(
                totalSupply,
                totalSupplyAfterMint,
                "Total supply dose not change with mint"
            );
            await basicLinearMarketInstance
                .from(userAccount.wallet)
                .mint(ethers.utils.parseUnits("1", 18));
            await EscrowInstance.from(userAccount.wallet).withdraw();
            let balanceOfEscrowAfter = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(EscrowInstance.contract.address);
            let balanceOfOwner = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(userAccount.wallet.address);
            assert.strictEqual(
                ethers.utils.formatUnits(balanceOfEscrowAfter, 18).toString(),
                "0.0",
                "Escrow has funds after withdraw"
            );
            assert.strictEqual(
                ethers.utils.formatUnits(balanceOfOwner, 18).toString(),
                "1000.0",
                "User dose not have all funds"
            );
            try {
                basicLinearMarketInstance
                    .from(userAccount.wallet)
                    .mint(ethers.utils.parseUnits("1", 18));
                    assert.equal(true, false, "User was able to mint past cap");
            } catch (error) {
                assert.equal(true, true, "");
            }
        });

        /**
         * Mints tokens and then burns them
         * Checks that the user gets paid back in PDAI after burning
         */
        it("Burning", async () => {
            let costToMint = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .priceToMint(ethers.utils.parseUnits("1000", 18));

            await PseudoDaiInstance
                .from(userAccount.wallet)
                .approve(
                    basicLinearMarketInstance.contract.address,
                    costToMint
                );

            await basicLinearMarketInstance
                .from(userAccount.wallet)
                .mint(ethers.utils.parseUnits("990", 18));

            let userPDAIBalance = await PseudoDaiInstance
                .from(userAccount.wallet)
                .balanceOf(userAccount.wallet.address);
            let userTokenBalance = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(userAccount.wallet.address);
            
            await basicLinearMarketInstance
            .from(userAccount.wallet)
            .burn(ethers.utils.parseUnits("900", 18));
            let userPDAIBalanceAfterBurn = await PseudoDaiInstance
                .from(userAccount.wallet)
                .balanceOf(userAccount.wallet.address);
            let userTokenBalanceAfterBurn = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(userAccount.wallet.address);
            assert.notEqual(
                userPDAIBalance,
                userPDAIBalanceAfterBurn,
                "User PDAI balance did not change with burn"
            );
            assert.notEqual(
                userTokenBalance,
                userTokenBalanceAfterBurn,
                "User token balance did not change with burn"
            );
        });

        /**
         * Mints some tokens and then sends them to another account
         * Checks both accounts balances change correctly 
         */
        it("Transferring", async () => {
            let costToMint = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .priceToMint(ethers.utils.parseUnits("1000", 18));

            await PseudoDaiInstance
                .from(userAccount.wallet)
                .approve(
                    basicLinearMarketInstance.contract.address,
                    costToMint
                );
            await basicLinearMarketInstance
                .from(userAccount.wallet)
                .mint(ethers.utils.parseUnits("990", 18));

            let balanceAfterMint = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(userAccount.wallet.address);

            let receiverBalanceBeforeTransfer = await basicLinearMarketInstance
                .from(adminAccount.wallet)
                .balanceOf(adminAccount.wallet.address);

            await basicLinearMarketInstance
                .from(userAccount.wallet)
                .transfer(
                    adminAccount.wallet.address,
                    ethers.utils.parseUnits("1", 18)
                );

            let balanceAfterTransfer = await basicLinearMarketInstance
                .from(userAccount.wallet)
                .balanceOf(userAccount.wallet.address);

            let reciverBalanceAfterTransfer = await basicLinearMarketInstance
                .from(adminAccount.wallet)
                .balanceOf(adminAccount.wallet.address);

            assert.notEqual(
                receiverBalanceBeforeTransfer,
                reciverBalanceAfterTransfer,
                "Receiving users balance did not change after transferring"
            );

            assert.notEqual(
                balanceAfterMint,
                balanceAfterTransfer,
                "Sending users balance did not change after transferring"
            );

            assert.notEqual(
                reciverBalanceAfterTransfer,
                balanceAfterTransfer,
                "User balance did not change after transferring"
            );
        });
    });
});