var MarketFactory = require('../build/MarketFactory.json');
var PseudoDaiToken = require('../build/PseudoDaiToken.json');
var NFTRegistry = require('../build/NFTRegistry.json');
var EscrowFactory = require('../build/EscrowFactory.json');
var BasicEscrowFactory = require('../build/BasicEscrowFactory.json');
var Escrow = require('../build/Escrow.json');

const etherlime = require('etherlime');

const pseudoDaiSettings = {
    name: "PseudoDai",
    symbol: "pDAI",
    decimals: 18
}

const nftRegistrySettings = {
    name: "The coolest tokens",
    symbol: "TCT",
    eventSig: {
        transfer: 'Transfer(address,address,uint256)'
    }
}

const moleculeData = {
    name: "caffeine",
    marketData: {
        tokenID: undefined,
        initialReserve: "1000",
        marketCap: "100000.0",
        escrowAddress: undefined
    }
}

describe('Escrow Factory test', () => {
    let deployer;
    let adminAccount = devnetAccounts[1];
    let userAccount = devnetAccounts[2];

    let PseudoDaiInstance,
        NFTRegistryInstance,
        MarketFactoryInstance,
        EscrowFactoryInstance,
        BasicEscrowFactoryInstance,
        EscrowInstance;

    /**
     * Sets up the needed contracts for testing.
     *     PDAI
     *     NFTRegistry 
     *     MarketFactory
     *     EscrowFactory
     *     BasicEscrowFactory
     * The basic factory is then linked up to 
     *     the root factory. 
     * An NFT is minted to the userAccount
     */
    beforeEach('', async () => {
        deployer = new etherlime.EtherlimeDevnetDeployer(adminAccount.secretKey);
        PseudoDaiInstance = await deployer.deploy(
            PseudoDaiToken,
            false,
            pseudoDaiSettings.name,
            pseudoDaiSettings.symbol,
            pseudoDaiSettings.decimals
        );
        NFTRegistryInstance = await deployer.deploy(
            NFTRegistry,
            false,
            nftRegistrySettings.name,
            nftRegistrySettings.symbol
        );
        MarketFactoryInstance = await deployer.deploy(
            MarketFactory,
            false,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );
        EscrowFactoryInstance = await deployer.deploy(
            EscrowFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address
        );
        BasicEscrowFactoryInstance = await deployer.deploy(
            BasicEscrowFactory,
            false,
            EscrowFactoryInstance.contract.address
        );
        await EscrowFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicEscrowFactoryInstance.contract.address);
        await NFTRegistryInstance
            .from(userAccount.wallet)
            .publishMolecule(moleculeData.name);
    });

    describe('Deploying an escrow', () => {
        /**
         * Checks that the deployed escrow has the correct
         * details
         */
        it("Deploying Escrow", async () => {
            let recipt = await (await EscrowFactoryInstance
                .from(userAccount.wallet)
                .deployEscrow(
                    0,
                    userAccount.wallet.address
                )).wait();
            let escrowAddress = await EscrowFactoryInstance
                .from(userAccount.wallet)
                .getEscrows(0);
            EscrowInstance = await etherlime.ContractAtDevnet(Escrow, escrowAddress[0]);
            assert.equal(
                recipt.events[0].args._nftOwner,
                userAccount.wallet.address,
                "Owner is incorrect"
            );
            assert.equal(
                recipt.events[0].args._newEscrow,
                EscrowInstance.contract.address,
                "Escrow address is incorrect"
            );
        });

        /**
         * Checks that the modifiers fail when they are supposed to:
         *      If the escrow factory type is invalid 
         *      If the user dose not own an NFT 
         */
        it("Testing modifiers", async () => {
            try {
                await assert.revert(
                    await EscrowFactoryInstance
                    .from(userAccount.wallet)
                    .deployEscrow(
                        1,
                        userAccount.wallet.address
                    )
                );
                assert.equal(true, false, "Escrow deployed with invalid factory");
            } catch (error) {
                assert.equal(true, true, "");
            }
            try {
                await assert.revert(
                    await EscrowFactoryInstance
                    .from(adminAccount.wallet)
                    .deployEscrow(
                        1,
                        adminAccount.wallet.address
                    )
                );
                assert.equal(true, false, "Escrow deployed without valid NFT owner");
            } catch (error) {
                assert.equal(true, true, "");
            }
        });

        /**
         * Checks the addresses in events and contract are the same
         */
        it("Getting deployed escrows", async () => {
            let recipt = await (await EscrowFactoryInstance
                .from(userAccount.wallet)
                .deployEscrow(
                    0,
                    userAccount.wallet.address
                )).wait();
            let deployedEscrowAddress = await EscrowFactoryInstance
                .from(userAccount.wallet)
                .getEscrows(
                    0
                );
            assert.equal(
                recipt.events[0].args._newEscrow,
                deployedEscrowAddress[0],
                "Escrow address in event and contract differ"
            );
        });
    });
});