/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
// tslint:disable-next-line:no-duplicate-imports
import Redux from 'redux';

// tslint:disable-next-line:no-submodule-imports
// tslint:disable-next-line:no-implicit-dependencies
import appReducer from 'containers/App/reducer';
import patentDomainReducer from 'domain/patent/reducer';
import exchangeDomainReducer from 'domain/exchange/reducer';
import authenticationReducer from 'domain/authentication/reducer';
import walletDomainReducer from 'domain/wallet/reducer';
/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer(injectedReducers: Redux.ReducersMapObject = {}): Redux.Reducer<any> {
  return combineReducers({
    authentication: authenticationReducer,
    patents: patentDomainReducer,
    exchange: exchangeDomainReducer,
    wallet: walletDomainReducer,
    ...injectedReducers,
  });
}
