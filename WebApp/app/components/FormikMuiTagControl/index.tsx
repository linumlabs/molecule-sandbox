/**
 *
 * FormikMuiTab
 *
 */

import React, { Fragment } from 'react';
import { Theme, createStyles, withStyles, WithStyles, Chip, InputLabel, FormHelperText } from '@material-ui/core';
// import ReactTags from 'react-tag-autocomplete';
import { FieldProps, getIn } from 'formik';
import ReactTags from 'react-materialui-tags';

interface OwnProps extends FieldProps {
  disabled: boolean;
}

//TODO consider allowing passing through props from parent to override things like allowNew

const ReactTagsInput = ReactTags.WithContext;

const FormikMuiTagControl: React.SFC<OwnProps> = ({ field, form, disabled }: OwnProps) => {
  if (!Array.isArray(field.value)) {
    console.log('Input field provided to Tag control is not an array');
    return <Fragment />
  }

  const { name } = field;
  const { touched, errors, setFieldValue, setFieldTouched, dirty } = form;

  const fieldError = getIn(errors, name);
  const showError = getIn(touched, name) && !!fieldError;

  return (
    <Fragment>
      <ReactTagsInput
        tags={field.value}
        placeholder='Add more'
        handleAddition={(tag) => {
          setFieldValue(field.name, field.value.concat([{ id: tag.text, text: tag.text }]))
          setFieldTouched(field.name, true);
        }}
        handleDelete={(i) => {
          setFieldValue(field.name, field.value.filter((tag, index) => index !== i))
          setFieldTouched(field.name, true);
        }}
        handleDrag={(tag, currPos, newPos) => {
          const tags = field.value;
          const newTags = tags.slice();
          newTags.splice(currPos, 1);
          newTags.splice(newPos, 0, tag);
          setFieldValue(field.name, newTags);
          setFieldTouched(field.name, true);
        }}
        handleInputBlur={() => {
          // TODO: Figure out a better check as this will fail if the tag control is the first control to be interacted with
          if (dirty) { setFieldTouched(field.name, true) }
        }}
        allowNew={true}
        readOnly={disabled} />
      {showError &&
        <FormHelperText error>
          {fieldError}
        </FormHelperText>
      }
    </Fragment>
  );
};

export default FormikMuiTagControl;
