/**
 *
 * BuySellCompoundTokens
 *
 */

import React, { Fragment } from 'react';
import { Theme, 
  createStyles, 
  withStyles, 
  WithStyles, 
  Typography, 
  Paper, 
  Button, 
  LinearProgress, 
  Divider, 
  Dialog,
  FormControl,
  Grid,
  DialogTitle,
  DialogActions,
  DialogContent,
  Snackbar} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { ActiveTab } from 'containers/BuySellCompoundContainer';
import { Form, Field, getIn } from 'formik';
import { TextField } from 'formik-material-ui';
import { ArrowBack, Save } from '@material-ui/icons';

const styles = ({ spacing, breakpoints, palette }: Theme) =>
  createStyles({
    layout: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: spacing.unit * 3,
      marginRight: spacing.unit * 3,
      marginTop: spacing.unit * 2,
      [breakpoints.up(400 + spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    form: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
      alignContent: 'center',
      [breakpoints.down('md')]: {
        flexDirection: 'column'
      }
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      overflowX: 'auto',
      width: '100%',
    },
    textField: {
      marginLeft: spacing.unit,
      marginRight: spacing.unit,
    },
    button: {
      width: '100%',
    },
    buttonText: {
      color: 'white',
    },
    rightText: {
      alignContent: 'right',
      alignItems: 'right',
      alignSelf: 'right',
    },
    iconMargin: {
      marginRight: `5px`
    },
    controls: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    buttonRight: {
      textAlign: 'right',
    },
    buttonLeft: {
      textAlign: 'left',
    },
    dialogContent: {
      padding: spacing.unit * 2,
    },
    gridItemRight: {
      textAlign: "right",
    }
  });

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

interface OwnProps extends WithStyles<typeof styles> {
  costToBuy: number;
  sellReward: number;
  handleChangeTab(value: ActiveTab): void;
  activeTab: ActiveTab;
  submitForm(): void;
  daiBalance: number;
  compoundTokenBalance: number;
  values: any;
  isValid: boolean;
  isSubmitting: boolean;
  onChange(e: any): void;
  handleChange(e: React.ChangeEvent<any>): void;
  handleBlur(e: any): void;
  buySellTransaction: boolean;
  buySellTransactionMessage: string | null;
}

type Props = OwnProps;

class BuySellCompoundTokens extends React.Component<Props> {
  state = {
    dialogOpen: false,
    snackbarOpen: false,
  }

  handleToggle = () => {
    this.setState({ dialogOpen: !this.state.dialogOpen });
  }

  handleChangeActiveTab = (event, value) => {
    this.props.handleChangeTab(value);
  }

  onKeyDownHandler = (e: React.KeyboardEvent) => {
    if(e.key === 'Enter') {
      e.preventDefault()
    }
  }

  componentDidUpdate(prevProps) {
    if(prevProps.buySellTransaction && !this.props.buySellTransaction) {
      this.setState({ snackbarOpen: true, dialogOpen: false });
    }
  }

  render() {
    const { 
      classes, 
      daiBalance, 
      compoundTokenBalance, 
      costToBuy,
      sellReward,
      activeTab, 
      submitForm,
      values,
      isValid,
      isSubmitting,
      onChange,
      handleChange,
      handleBlur,
      buySellTransaction,
      buySellTransactionMessage,
    } = this.props;

    let totalCost = activeTab == ActiveTab.BuyTab ? costToBuy : sellReward;

    return (
      <Form className={classes.form} onKeyDown={this.onKeyDownHandler}>
        <Fragment>
          <section className={classes.layout}>
            <AppBar position="static">
              <Tabs
                value={activeTab}
                onChange={this.handleChangeActiveTab}
                variant="fullWidth">
                <Tab label="Buy" value={ActiveTab.BuyTab}/>
                <Tab label="Sell" value={ActiveTab.SellTab}/>
              </Tabs>
              <Paper className={classes.container}>
                <TabContainer>
                  <Grid container spacing={24}>
                    <Grid item xs={12} sm={6}>
                      <Typography variant="h6">Your Balance:</Typography>
                    </Grid>
                    <Grid className={classes.gridItemRight} item xs={12} sm={6}>
                      <Typography variant="h6">
                        {activeTab === ActiveTab.BuyTab ? 
                          `$ ${daiBalance}` : 
                          `${compoundTokenBalance} Tokens`
                        }
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography variant="h6">
                        Token Amount:
                      </Typography>
                    </Grid>
                    <Grid className={classes.gridItemRight} item xs={12} sm={6}>
                      <Field
                        name="tokenAmount"
                        type="number"
                        label="Token Amount"
                        component={TextField}
                        disabled={isSubmitting}
                        InputProps={{ 
                          onBlur: e => handleBlur(e),
                          onChange: e => {handleChange(e); onChange(e)}
                        }}/>
                      {/* <ErrorMessage name="tokenAmount">
                        {errorMessage => <div>{errorMessage}</div>}
                      </ErrorMessage> */}
                    </Grid>
                    <Grid item xs={12}>
                      <Divider variant="middle"/>
                      {isSubmitting && <LinearProgress color={"secondary"}/>}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography variant="h6">
                        Total:
                      </Typography>
                    </Grid>
                    <Grid className={classes.gridItemRight} item xs={12} sm={6}>
                      <Typography variant="h6" color={isSubmitting ? 'textSecondary' : 'textPrimary'}>
                        ${totalCost}
                      </Typography>
                    </Grid>
                    <Grid container justify="space-around" alignItems="center">
                      <Grid item xs={12} sm={6}>
                        <Button
                          variant="contained"
                          color="secondary"
                          className={classes.button}
                          onClick={this.handleToggle}
                          disabled={isSubmitting || !isValid}>
                          <Typography variant="h6" className={classes.buttonText}>
                            {activeTab === ActiveTab.BuyTab ? 'Buy' : 'Sell'}
                          </Typography>
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </TabContainer>
              </Paper>
            </AppBar>
          </section>
          <Dialog
            open={this.state.dialogOpen}
            onClose={() => {if(!buySellTransaction) this.handleToggle()}}>
            <DialogTitle>
              <Typography>
                Are you sure you want to {activeTab === ActiveTab.BuyTab ? 'buy' : 'sell'} {getIn(values, 'tokenAmount')} tokens for ${totalCost}?
              </Typography>
            </DialogTitle>
            <DialogContent>
              {buySellTransaction && <LinearProgress />}
            </DialogContent>
            <DialogActions>
              <Grid container justify={'space-between'}>
                <Grid item xs={5}>
                   <FormControl required fullWidth>
                    <Button
                      disabled={buySellTransaction} 
                      variant="outlined"
                      color="secondary"
                      onClick={this.handleToggle}>
                      <ArrowBack className={classes.iconMargin} />
                      Return
                    </Button>
                  </FormControl>
                </Grid>
                <Grid item xs={5}>
                  <FormControl required fullWidth>
                    <Button
                      disabled={buySellTransaction}
                      variant="outlined"
                      color="primary"
                      onClick={submitForm}>
                      <Save className={classes.iconMargin} />
                      Submit
                    </Button>
                  </FormControl>
                </Grid>
              </Grid>
            </DialogActions>
          </Dialog>
        </Fragment>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.snackbarOpen}
          autoHideDuration={10000}
          onClose={() => this.setState({ snackbarOpen: false })}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{buySellTransactionMessage}</span>}
        />
      </Form>
    );
  };
}

export default withStyles(styles, { withTheme: true })(BuySellCompoundTokens);
