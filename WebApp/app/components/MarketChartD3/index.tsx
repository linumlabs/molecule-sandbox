/**
 *
 * MarketChartD3
 *
 */

import React, { Fragment } from 'react';
import { Theme, createStyles, withStyles, WithStyles, Paper } from '@material-ui/core';
import * as d3 from "d3";
import './d3Style.css';

const styles = (theme: Theme) =>
  createStyles({
    layout: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    container: {
      boxShadow: 'none',
    },
    chartDiv: {
      padding: theme.spacing.unit * 2,
    },
  });

interface OwnProps extends WithStyles < typeof styles > {
  currentTokenValue: number;
  currentTokenSupply: number;
  marketCap: number;
  amountInEscrow: number;
};

class MarketChartD3 extends React.Component < OwnProps > {

    _rootNode;
    _svgNode;

    renderChart() {
      if (!this._rootNode) return;

      const {
        currentTokenValue,
        currentTokenSupply,
        marketCap,
        amountInEscrow
      } = this.props;

      // D3 Code to create the chart
      // using this._rootNode as container
      // line & graph parameters
      let market_cap = marketCap,
        current_supply = currentTokenSupply,
        initial_supply = amountInEscrow,
        current_price = currentTokenValue,
        slope = current_price / (current_supply + 1 - initial_supply),
        y_intercept = slope * -1 * initial_supply;

      let min_mint = market_cap/1000;

      // set the dimensions and margins of the graph
      let margin = {
          top: 20,
          right: 70,
          bottom: 70,
          left: 100
        },
        width = this._rootNode.offsetWidth - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

      // aggregate line data points in array of objects
      let data = d3.range(initial_supply, market_cap + 1, min_mint).map(
        function (d) {
          return {
            "x": d,
            "y": d * slope + y_intercept
          }
        });

      // data array to draw area below curve
      let area_data = d3.range(initial_supply, current_supply - initial_supply + 1, min_mint).map(
        function (d) {
          return {
            "x": d,
            "y": d * slope + y_intercept
          }
        });

      // helper function to calculate optimum y-tick settings
      function getSmartTicks(max_y) {
        //base step between nearby two ticks
        let step = Math.pow(2, Math.round(max_y).toString().length - 1);

        //add one more step if the last tick value is the same as the max value
        //if you don't want to add, remove "+1"
        let slices_count = Math.ceil((max_y + 1) / step);

        return {
          end_point: slices_count * step,
          count: Math.min(10, slices_count) //show max 10 ticks
        };
      }

      // data.map(a => a.y)) create array of y-values
      let y_tick_settings = getSmartTicks(d3.max(data.map(a => a.y)));

      // set the ranges
      let xscale = d3.scaleLinear()
        .domain([0, market_cap])
        .range([0, width]);

      let yscale = d3.scaleLinear()
        .domain([0, y_tick_settings.end_point])
        .range([height, 0]);

      // line generator
      let line = d3.line()
        .x(function (d) {
          return xscale(d.x);
        })
        .y(function (d) {
          return yscale(d.y);
        })
        .curve(d3.curveMonotoneX); // apply smoothing to the line

      // grid line functions
      function makexGridlines() {
        return d3.axisBottom(xscale)
      };

      function makeyGridlines() {
        return d3.axisLeft(yscale)
      };

      // area generator (area = collateral)
      let area = d3.area()
        .x(function (d) {
          return xscale(d.x);
        })
        .y0(height)
        .y1(function (d) {
          return yscale(d.y);
        });

      // append the svg obgect to the body of the page
      if(this._svgNode) {
        this._rootNode.childNodes.forEach(node => {
          node.remove();
        });
      }

      this._svgNode = d3.select(this._rootNode)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // add gridlines to graph
      this._svgNode.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .style("stroke-dasharray", ("3,3"))
        .call(makexGridlines()
          .tickSize(-height)
          .tickFormat("")
        );

      this._svgNode.append("g")
        .attr("class", "grid")
        .style("stroke-dasharray", ("3,3"))
        .call(makeyGridlines()
          .ticks([y_tick_settings.count])
          .tickSize(-width)
          .tickFormat("")
        );

      // add x- and y-axis
      this._svgNode.append("g")
        .attr("transform", "translate(0, " + height + ")")
        .call(d3.axisBottom().scale(xscale));

      this._svgNode.append("g")
        .call(d3.axisLeft().scale(yscale).ticks(y_tick_settings.count));

      // axis titles
      this._svgNode.append("text")
        .attr("class", "label")
        .attr("text-anchor", "middle")
        .attr("transform", "translate(" + -1 * (margin.left / 3 * 2) + "," + (height / 2) + ")rotate(-90)")
        .text("Price [$]");

      this._svgNode.append("text")
        .attr("class", "label")
        .attr("text-anchor", "middle")
        .attr("transform", "translate(" + (width / 2) + "," + (height + margin.bottom / 3 * 2) + ")")
        .text("Supply");

      // fill area below line to indicate current market cap
      this._svgNode.append("path")
        .datum(area_data)
        .attr("class", "area")
        .attr("d", area);

      // add line to grah
      this._svgNode.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("d", line);

      // append group holding tooltip elements
      let tooltipGroup = this._svgNode.append("g")
        .attr("class", "tooltip-group")
        .style("display", "none");

      // append line to show x-coordinate of tooltip
      tooltipGroup.append("line")
        .attr("class", "x-line")
        .attr("y1", 0)
        .attr("y2", height);

      // append line to show y-coordinate of tooltip
      tooltipGroup.append("line")
        .attr("class", "y-line")
        .attr("x1", width)
        .attr("x2", width);

      // text to display token price at mouse location
      tooltipGroup.append("text") // white shade to make text legible when over line
        .attr("class", "label-shade y-label-shade")
        .attr("dx", 8)
        .attr("dy", "-.3em");
      tooltipGroup.append("text") // actual text
        .attr("class", "label-text y-label-text")
        .attr("dx", 8)
        .attr("dy", "-.3em");

      // text to display date at mouse location
      tooltipGroup.append("text")
        .attr("class", "label-shade x-label-shade")
        .attr("dx", 8)
        .attr("dy", "1em");
      tooltipGroup.append("text")
        .attr("class", "label-text x-label-text")
        .attr("dx", 8)
        .attr("dy", "1em");

      // add circle around data point closest to mouse pointer
      tooltipGroup.append("circle")
        .attr("class", "tooltip-circle")
        .style("fill", "none")
        .style("stroke", "blue")
        .attr("r", 4);

      // format displayed numbers for human consumption
      let formatNumber = d3.format(",.0f")

      // function to select data point based on mouse position
      function mouseMove(context: MarketChartD3) {
        let x0 = xscale.invert(d3.mouse(d3.event.currentTarget)[0]); // x-coordinate of mouse pointer
        let i = d3.bisector(function (d) {
          return d.x;
        }).left(data, x0, 1);
        let d0 = data[i - 1];
        let d1 = data[i];
        let d = x0 - d0.supply > d1.supply - x0 ? d1 : d0;

        // move x-line to data point closest to mouse location
        tooltipGroup.select("line.x-line")
          .attr("transform",
            "translate(" + xscale(d.x) + "," + yscale(d.y) + ")")
          .attr("y2", height - yscale(d.y));

        // move y-line to data point closest to mouse location
        tooltipGroup.select("line.y-line")
          .attr("transform",
            "translate(" + -1 * width + "," + yscale(d.y) + ")")
          .attr("x2", width + xscale(d.x));

        // move tooltip circle to data point closest to mouse location
        tooltipGroup.select("circle.tooltip-circle")
          .attr("transform",
            "translate(" + xscale(d.x) + "," + yscale(d.y) + ")");

        // move token price text to data point closest to mouse location
        tooltipGroup.select("text.y-label-shade")
          .attr("transform",
            "translate(" + (xscale(d.x)-150) + "," + yscale(d.y) + ")")
          .text("Price:\t\t\t " + " $" + formatNumber(d.y));

        tooltipGroup.select("text.y-label-text")
          .attr("transform",
            "translate(" + (xscale(d.x)-150) + "," + yscale(d.y) + ")")
          .text("Price:\t\t\t " + " $" + formatNumber(d.y));

        // move token supply text to data point closest to mouse location
        tooltipGroup.select("text.x-label-shade")
          .attr("transform",
            "translate(" + (xscale(d.x)-150) + "," + yscale(d.y) + ")")
          .text("Supply: " + formatNumber(d.x));

        tooltipGroup.select("text.x-label-text")
          .attr("transform",
            "translate(" + (xscale(d.x)-150) + "," + yscale(d.y) + ")")
          .text("Supply: " + formatNumber(d.x));
      }

      // rectangle to catch mouse movement
      this._svgNode.append("rect")
        .attr("width", width)
        .attr("height", height)
        .style("fill", "none")
        .style("pointer-events", "all")
        .on("mouseover", function () {
          tooltipGroup.style("display", null);
        })
        .on("mouseout", function () {
          tooltipGroup.style("display", "none");
        })
        .on("mousemove", () => {
          mouseMove(this)
        })
  }

  constructor(props) {
    super(props);
    this.renderChart = this.renderChart.bind(this);
  }

  shouldComponentUpdate() {
    return true;
  }

  _setRef(componentNode) {
    this._rootNode = componentNode;
    // remove old nodes
    if(this._rootNode) {
      this._rootNode.childNodes.forEach(node => {
        node.remove();
      });
    }
    this.renderChart();
  }

  render() {
    const { classes } = this.props;

    return (
      <Fragment>
        <section className={classes.layout}>
          <Paper className={classes.container} square>
            <div className={classes.chartDiv}>
              <div ref={this._setRef.bind(this)} />
            </div>
          </Paper>
        </section>
      </Fragment>
    );
  }
};

export default withStyles(styles, { withTheme: true })(MarketChartD3);
