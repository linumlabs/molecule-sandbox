/**
 *
 * PatentSummary
 *
 */

import React, { Fragment } from 'react';
import { Theme, createStyles, withStyles, WithStyles, Paper, Chip, Typography, Button } from '@material-ui/core';
import { forwardTo } from 'utils/history';

const styles = (theme: Theme) =>
  createStyles({
      banner: {
        backgroundColor: '#cfd8dc',
      },
      layout: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
          marginLeft: 'auto',
          marginRight: 'auto',
        },
      },
      container: {
        marginTop: theme.spacing.unit * 3,
      },
      componentTitle: {
        padding: theme.spacing.unit * 2,
      },
      content: {
        padding: theme.spacing.unit * 3,
      },
      title: {
        overflowX: 'hidden',
        textOverflow: 'ellipses',
      },
      therapeuticArea: {
        fontWeight: 'bold',
        display: 'inline-block',
      },
      chip: {
        margin: theme.spacing.unit * 2,
        float: 'right',
      },
      uidButton: {
        margin: theme.spacing.unit * 2
      }
  });

enum DrugType {
  SmallMolecule,
  BiotechDrug
}

interface PatentSummary {
  uid: string,
  title: string,
  abstract: string,
  drugType: DrugType,
  therapeuticArea: string,
}

interface OwnProps extends WithStyles<typeof styles> {
  id: string,
  patentSummary: PatentSummary,
  marketAddress: string,
}

const PatentSummary: React.SFC<OwnProps> = (props: OwnProps) => {
  const {classes, id, patentSummary, marketAddress} = props;

  const truncate = (text: string, characters: number) => {
    return `${text.substring(0, characters)}${text.length > characters ? '...':''}`
  }

  const renderDrugType = (drugType: DrugType) => {
    switch (drugType) {
      case DrugType.SmallMolecule.valueOf(): return "Small Molecule"
      case DrugType.BiotechDrug.valueOf(): return "Biotech Drug"
      default: return "Unknown type";
    }
  }

  return (
    <Fragment>
      <section className={classes.layout}>
        <Paper className={classes.container}>
          <Typography className={classes.componentTitle} variant="h5">
            Patent Summary
          </Typography>
          <div className={classes.banner}>
            <Button className={classes.uidButton} variant="outlined" 
                onClick={() => window.open("https://rinkeby.etherscan.io/tx/" + marketAddress, "_blank")}>
              Market Contract
            </Button>
            <Button className={classes.uidButton} variant="outlined" onClick={() => forwardTo(`/patent/${id}`)}>{patentSummary.uid}</Button>
            <Chip className={classes.chip} color="primary" label={patentSummary.therapeuticArea}/>
            <Chip className={classes.chip} color="secondary" label={renderDrugType(patentSummary.drugType)}/>
          </div>
          <div className={classes.content}>
            <Typography variant="title" className={classes.title} gutterBottom noWrap>
              {patentSummary.title}
            </Typography>
            <Typography variant="body1" paragraph gutterBottom>
              {truncate(patentSummary.abstract, 500)}
            </Typography>
            <Typography className={classes.therapeuticArea} variant="subtitle1" inline>
              Therapeutic Area:&nbsp;
            </Typography>
            <Typography variant="subtitle1" inline>{patentSummary.therapeuticArea}</Typography>
          </div>
        </Paper>
      </section>
    </Fragment>
  );
};

export default withStyles(styles, { withTheme: true })(PatentSummary);
