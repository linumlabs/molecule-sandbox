/**
 *
 * TransactionHistory
 *
 */

import React, { Fragment } from 'react';
import { Theme, createStyles, withStyles, WithStyles, Paper, Table, TableHead, TableRow, TableCell, TableBody, Button, Typography, CircularProgress, TableFooter, TablePagination, PropTypes, IconButton } from '@material-ui/core';
import { blueGrey } from '@material-ui/core/colors';
import { TransactionDatapoint } from 'containers/ExchangePage/dto/TransactionDatapoint';
import { KeyboardArrowRight, KeyboardArrowLeft, LastPage, FirstPage } from '@material-ui/icons';

const styles = ({ spacing, breakpoints, palette }: Theme) => createStyles({
    layout: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: spacing.unit * 3,
      marginRight: spacing.unit * 3,
      [breakpoints.up(400 + spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    container: {
      marginTop: spacing.unit * 2,
      display: 'flex',
      flexDirection: 'column',
      overflowX: 'auto',
      width: '100%',
    },
    tableTitle: {
      padding: spacing.unit * 2,
    },
    tableRow: {
      cursor: 'pointer',
      '&:nth-of-type(even)': {
        backgroundColor: palette.background.default,
      },
    },
    tableHeadCell: {
      fontSize: '1rem',
    },
    header: {
      backgroundColor: blueGrey[100],
    },
    buy: {
      color: "green",
    },
    sell: {
      color: "red",
    },
    button: {
      margin: spacing.unit,
      backgroundColor: blueGrey[50],
    },
    linkStyling: {
      underline: "none",
      target: "_blank",
    },
    progress: {
      margin: spacing.unit * 2,
      position: 'relative',
      marginLeft: '50%',
    },
    placeHolder: {
      margin: spacing.unit
    }
});

interface OwnProps extends WithStyles<typeof styles> {
  display: boolean,
  transactions: Array<TransactionDatapoint>;
}

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});

interface ActionProps {
  classes: any,
  count: number,
  onChangePage: any,
  page: number,
  rowsPerPage: number,
  theme: any,
};


class TablePaginationActions extends React.Component<ActionProps> {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === 'rtl' ? <LastPage /> : <FirstPage />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === 'rtl' ? <FirstPage /> : <LastPage />}
        </IconButton>
      </div>
    );
  }
}

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  TablePaginationActions,
);

class TransactionHistory extends React.Component<OwnProps> {
  state = {
    page: 0,
    rowsPerPage: 5,
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  };
  
  handleChangeRowsPerPage = (event) => {
    this.setState({ page: 0, rowsPerPage: event.target.value });
  }

  render() {
    const { classes, transactions, display } = this.props;
    const { rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, transactions.length - page * rowsPerPage);
    const rows = transactions && transactions
      .filter(entry => Math.abs(entry.daiAmount) > 0)
      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

    return (
      <Fragment>
        <section className={classes.layout}>
          <Paper className={classes.container}>
            <Typography className={classes.tableTitle} variant="h5">
              Transaction History
            </Typography>
            <Table>
              <TableHead className={classes.header}>
                <TableRow>
                  <TableCell className={classes.tableHeadCell}>Type</TableCell>
                  <TableCell className={classes.tableHeadCell}>Volume</TableCell>
                  <TableCell className={classes.tableHeadCell}>Initial Price ($)</TableCell>
                  <TableCell className={classes.tableHeadCell}>Value ($)</TableCell>
                  <TableCell className={classes.tableHeadCell}>Time</TableCell>
                </TableRow>
              </TableHead>
              {display &&
                <TableBody>
                  {rows.map(row => (
                    <TableRow key={row.transactionHash} className={classes.tableRow}
                      hover
                      onClick={() => window.open("https://rinkeby.etherscan.io/tx/" + row.transactionHash, "_blank")}>
                      <TableCell className={ row.daiAmount <= 0 ? classes.buy : classes.sell} >
                        {row.daiAmount <= 0 ? "Buy" : "Sell"}
                      </TableCell>
                      <TableCell>{row.tokenAmount}</TableCell>
                      <TableCell>{row.firstTokenPrice}</TableCell>
                      <TableCell>{Math.abs(row.daiAmount)}</TableCell>
                      <TableCell>{(new Date(Number(row.timestamp)*1000)).toLocaleString()}</TableCell>
                    </TableRow>
                  ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 48 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>}
                {display && rows.length > 0 && <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25]}
                      colSpan={6}
                      count={transactions.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      SelectProps={{
                        native: true,
                      }}
                      onChangePage={this.handleChangePage}
                      onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      ActionsComponent={TablePaginationActionsWrapped}
                    />
                  </TableRow>
                </TableFooter>}
              </Table>
              {!display && <CircularProgress className={classes.progress} />}
              {display && rows.length === 0 && <Typography variant="h6" className={classes.placeHolder}>No valid transactions.</Typography>}
          </Paper>
        </section>
      </Fragment>
    );
  }
};

export default withStyles(styles, { withTheme: true })(TransactionHistory);
