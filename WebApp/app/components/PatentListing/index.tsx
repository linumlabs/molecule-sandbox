/**
 *
 * PatentListing
 *
 */

import {
  Avatar,
  Button,
  createStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Theme,
  WithStyles,
  withStyles,
  Typography,
} from '@material-ui/core';
import EyeIcon from '@material-ui/icons/RemoveRedEye';
import apiUrlBuilder from 'api/apiUrlBuilder';
import React from 'react';
import { Link } from 'react-router-dom';

const styles = ({ spacing, breakpoints }: Theme) => createStyles({
  avatar: {
    margin: 10,
  },
  nameCell: {
    maxWidth: 300,
    overflowX: 'hidden',
    textOverflow: 'ellipses',
  },
  linkIcon: {
    textDecoration: 'none',
    color: 'black',
  }
});

interface OwnProps extends WithStyles<typeof styles> {
  patents: any[];
}

const PatentListing: React.SFC<OwnProps> = (props: OwnProps) => {
  const { classes } = props;

  return (
    <Paper>
      <section>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <Link to={'/patent'} className={classes.linkIcon}>
                  <Button
                    variant="outlined"
                    title="Create">
                    Create
                  </Button>
                </Link>
              </TableCell>
              <TableCell>Title</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Type</TableCell>
              <TableCell>Date</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.patents &&
              props.patents.map((row, index) => (
                <TableRow key={row.id}>
                  <TableCell>
                    <Avatar src={apiUrlBuilder.attachmentStream(row.relevantVisualization)} className={classes.avatar} />
                  </TableCell>
                  <TableCell className={classes.nameCell}>
                    <Typography noWrap>{row.title}</Typography>
                  </TableCell>
                  <TableCell>{row.status}</TableCell>
                  <TableCell>{row.type}</TableCell>
                  <TableCell>{row.createdAt}</TableCell>
                  <TableCell>
                    <Link to={`/patent/${row.id}`} className={classes.linkIcon}>
                      <EyeIcon />
                    </Link>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </section>
    </Paper>
  );
};

export default withStyles(styles)(PatentListing);
