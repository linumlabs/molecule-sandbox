/**
 *
 * CreateNftForm
 *
 */

import React, { Fragment } from 'react';
import { Theme, 
  createStyles, 
  withStyles, 
  WithStyles, 
  Button, 
  FormControl, 
  Typography, 
  LinearProgress} from '@material-ui/core';
import { Form, Field } from 'formik';
import { Save, ArrowBack } from '@material-ui/icons';
import { TextField } from 'formik-material-ui';

const styles = ({ spacing, breakpoints }: Theme) => createStyles({
  layout: {
    width: 'auto',
    display: 'grid',
  },
  form: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignContent: 'center',
    marginTop: spacing.unit * 2,
    marginBottom: spacing.unit * 2,
    marginLeft: spacing.unit * 2,
    marginRight: spacing.unit * 2,
    padding: spacing.unit * 2,
    [breakpoints.down('md')]: {
      flexDirection: 'column'
    }
  },
  iconMargin: {
    marginRight: `5px`
  },
  controls: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  buttonRight: {
    marginTop: spacing.unit * 2,
    alignItems: 'right',
  },
  buttonLeft: {
    marginTop: spacing.unit * 2,
    alignItems: 'left',
  }
});

interface OwnProps extends WithStyles<typeof styles> {
  submitForm(): void;
  closeDialog(): void;
  isSubmitting: boolean;
}

const CreateNftForm: React.SFC<OwnProps> = (props: OwnProps) => {
  const { classes, submitForm, closeDialog, isSubmitting } = props;

  return (
    <Form className={classes.form}>
      <Typography component="h1" variant="h6">
        Are you sure you want to tokenize? 
        Patent details can no longer be adjusted after the asset has been tokenized.
      </Typography>
      <FormControl margin='normal' required fullWidth>
      {isSubmitting && <LinearProgress />}
        <div className={classes.controls}>
          <Field name="patentId" label="id" component={TextField} style={{ display: 'none' }} />  
          <Button 
              variant="contained"
              color="secondary"
              className={classes.buttonLeft}
              disabled={isSubmitting}
              onClick={closeDialog}>
              <ArrowBack className={classes.iconMargin} />
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={classes.buttonRight}
              disabled={isSubmitting}
              onClick={submitForm}>
              <Save className={classes.iconMargin} />
              Continue
          </Button>
        </div>
      </FormControl>
    </Form>
  );
};

export default withStyles(styles, { withTheme: true })(CreateNftForm);
