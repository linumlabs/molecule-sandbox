import { Paper, Theme, Typography } from '@material-ui/core';
import { createStyles, withStyles } from '@material-ui/core/styles';
import React, { Fragment } from 'react';

const styles = ({ spacing, breakpoints }: Theme) => createStyles({
  layout: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: spacing.unit * 3,
    marginRight: spacing.unit * 3,
    [breakpoints.up(400 + spacing.unit * 3 * 2)]: {
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${spacing.unit * 2}px ${spacing.unit * 3}px ${spacing.unit * 3}px`,
  },
  welcomeMessage: {
    padding: `${spacing.unit * 2}px ${spacing.unit * 3}px ${spacing.unit * 3}px`,
    color: 'white',
  },
  logoFull: {
    height: '10em',
  },
});

function LandingPage(props) {
  const { classes } = props;

  return (
  <Fragment>
    <main className={classes.layout}>
      <img className={classes.logoFull} src="https://i.imgur.com/6HDY4yZ.png"/>
      {/*<Typography className={classes.welcomeMessage} variant='h2'>Molecule Sandbox</Typography>*/}
      <Typography className={classes.welcomeMessage} variant="h4">The next evolution
        of drug development</Typography>
      <Typography className={classes.welcomeMessage} variant="h6">Molecule is a software platform to
        accelerate innovation in the pharmaceutical industry. <br/> It connects scientists,
        patients and industry to advance drug development in a collaborative open market.
      </Typography>
    </main>
  </Fragment>
  );
}

export default withStyles(styles, { withTheme: true })(LandingPage);
