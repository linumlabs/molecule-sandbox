/**
 *
 * MarketChartLayout
 *
 */

import React from 'react';
import { withStyles, WithStyles, createStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import MarketHistoryChart from 'components/MarketHistoryChart';
import MarketChartD3 from 'components/MarketChartD3';
import { TransactionDatapoint } from 'containers/ExchangePage/dto/TransactionDatapoint';
import { Chip, Paper, CircularProgress } from '@material-ui/core';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      overflowX: 'auto',
      width: '100%',
    },
    charts: {
      backgroundColor: '#cfd8dc',
    },
    chip: {
      margin: theme.spacing.unit * 2
    },
    progress: {
      margin: theme.spacing.unit * 2,
      position: 'relative',
      marginLeft: '50%',
    },
  });

interface OwnProps extends WithStyles<typeof styles> {
  display: boolean,
  marketSupplyProps: {
    currentTokenValue: number;
    currentTokenSupply: number;
    marketCap: number;
    amountInEscrow: number;
  },
  marketHistoryProps: {
    currentTokenValue: number;
    currentTokenSupply: number;
    marketCap: number;
    amountInEscrow: number;
    marketHistory: TransactionDatapoint[];
  },
};

class MarketChartLayout extends React.Component<OwnProps> {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, marketSupplyProps, marketHistoryProps, display } = this.props;
    const { value } = this.state;

    return (
      <Paper className={classes.root}>
        <AppBar position="static" color="inherit">
          <Tabs value={value} onChange={this.handleChange}>
            <Tab label="Supply" />
            <Tab label="Price" />
          </Tabs>
        </AppBar>
        <section className={classes.charts}>
          <Chip className={classes.chip}
            label={`Token value: ${marketSupplyProps.currentTokenValue}`}
            color="primary"
          />
          <Chip className={classes.chip}
            label={`Token supply: ${marketSupplyProps.currentTokenSupply}`}
            color="primary"
          />
          <Chip className={classes.chip}
            label={`Market cap: ${marketSupplyProps.marketCap}`}
            color="primary"
          />
          { value === 0 && marketSupplyProps.marketCap > 0 ?
              <MarketChartD3
                currentTokenValue={marketSupplyProps.currentTokenValue}
                currentTokenSupply={marketSupplyProps.currentTokenSupply}
                marketCap={marketSupplyProps.marketCap}
                amountInEscrow={marketSupplyProps.amountInEscrow}/>
              : value === 0 && <CircularProgress className={classes.progress} />
          }
          { value === 1 && display ?
              <MarketHistoryChart
                currentTokenValue={marketHistoryProps.currentTokenValue}
                currentTokenSupply={marketHistoryProps.currentTokenSupply}
                marketCap={marketHistoryProps.marketCap}
                amountInEscrow={marketHistoryProps.amountInEscrow}
                marketHistory={marketHistoryProps.marketHistory}/>
              : value === 1 && <CircularProgress className={classes.progress} />
          }
        </section>
      </Paper>
    );
  }
}

export default withStyles(styles)(MarketChartLayout);