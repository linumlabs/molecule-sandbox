/**
 *
 * UploadedPdfIcon
 *
 */

import React, { Fragment } from 'react';
import { Theme, createStyles, withStyles, WithStyles, Typography, Dialog, Button } from '@material-ui/core';
import { AttachFile, Clear } from '@material-ui/icons';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
import apiUrlBuilder from 'api/apiUrlBuilder';

const styles = (theme: Theme) =>
  createStyles({
    fileIcon: {
      width: '56px',
      padding: '5px'
    },
    fileName: {
      width: '56px',
      overflowX: 'hidden',
      textOverflow: 'ellipses',
    },
    deleteIcon: {
      color: 'red'
    }
  });

interface OwnProps extends WithStyles<typeof styles> {
  file: any,
  onDelete(): void, 
}

interface OwnState {
  open: boolean,
  numPages: number,
  pageNumber: number,
}

class UploadedPdfIcon extends React.Component<OwnProps, OwnState> {
  public state = {
    open: false,
    numPages: 0,
    pageNumber: 1,
  };

  public onDocumentLoadSuccess = (document) => {
    const { numPages } = document;
    this.setState({
      numPages: numPages,
      pageNumber: 1,
    });
  };

  public changePage = offset => this.setState((prevState: any) => ({
    pageNumber: prevState.pageNumber + offset,
  }));

  public previousPage = () => this.changePage(-1);

  public nextPage = () => this.changePage(1);

  public toggleDialog = () => {
    this.setState({ open: !this.state.open });
  }

  render() { 
    const {classes, onDelete} = this.props;
    const file = typeof(this.props.file) === 'object' ? this.props.file : apiUrlBuilder.attachmentStream(this.props.file);
    const {open, pageNumber, numPages} = this.state;
    return (
    <div className={classes.fileIcon}>
      <AttachFile onClick={this.toggleDialog} fontSize='large' />
      <Typography noWrap className={classes.fileName}>{file && file.name}</Typography>
      <Dialog
          open={open}
          onClose={this.toggleDialog}
          maxWidth={'lg'}>
          <Document
            file={file}
            onLoadSuccess={this.onDocumentLoadSuccess} >
            <Page pageNumber={pageNumber} />
          </Document>
          {(numPages > 1) &&
            <div>
              <Button
                disabled={pageNumber <= 1}
                onClick={this.previousPage}
                variant="contained">
                Previous
              </Button>
              <Button
                disabled={pageNumber >= numPages}
                onClick={this.nextPage}
                variant="contained">
                Next
              </Button>
            </div>
          }
          { (typeof(file) === 'string') &&
            <a href={file} target="_blank">
              <Button>Download</Button>
            </a>
          }
        </Dialog>
    </div>
  )};
};

export default withStyles(styles, { withTheme: true })(UploadedPdfIcon);
