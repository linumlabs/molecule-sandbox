/**
 *
 * Wallet
 *
 */

import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  createStyles,
  Link as MuiLink,
  Theme,
  Typography,
  Grid,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  Table,
  TableBody,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import React, { Fragment } from 'react';
import apiUrlBuilder from 'api/apiUrlBuilder';
import { Link } from 'react-router-dom';
import { forwardTo } from 'utils/history';
import { IPatent } from 'domain/patent/types';
import { CompoundTokenInformation } from 'containers/WalletPage/types';

const styles = ({ spacing, breakpoints }: Theme) => createStyles({
  card: {
    maxWidth: '100%',
  },
  media: {
    height: 140
  },
  paper: {
    padding: spacing.unit,
  },
  linkIcon: {
    textDecoration: 'none',
    color: 'black',
  }
});

interface OwnProps extends WithStyles<typeof styles> {
  daiBalance: number;
  walletAddress: string;
  compounds: Array<CompoundTokenInformation>;
  nfts: Array<IPatent>;
  escrows: Array<CompoundTokenInformation>;
}

// TODO: Ensure that etherscan link works for other networks. 
const Wallet: React.SFC<OwnProps> = (props: OwnProps) => {
  const { classes, daiBalance, walletAddress, compounds, nfts, escrows } = props;
  return (
    <Fragment>
      <Grid container spacing={16}>
        <Grid item xs={12} md={6}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <Card>
                  <CardActionArea>
                    <CardContent>
                      <Typography variant='h5'>
                        {`Balance: ${daiBalance || 0} DAI`}
                      </Typography>
                      <Typography>
                        {walletAddress}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                    <Button size="small" color="primary">
                      View History
                    </Button>
                    <Button size="small" color="primary">
                      Withdraw
                    </Button>
                    <Button size="small" color="primary">
                      Deposit
                    </Button>
                  </CardActions>
                </Card>
              </Paper>
            </Grid>
            <Grid item xs={6}>
              <Paper className={classes.paper}>
                <Typography variant='h5'>NFTs</Typography>
                <Grid container direction='column'>
                  {nfts && nfts.map(nft => (
                    <Grid item key={nft.id}>
                      <Card className={classes.card}>
                        <CardActionArea>
                          <CardMedia
                            className={classes.media}
                            image={typeof nft.relevantVisualization === 'string' ? apiUrlBuilder.attachmentStream(nft.relevantVisualization) : undefined}
                            title={nft.title} />
                          <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                              {nft.title}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                        <CardActions>
                          <Link to={`/patent/${nft.id}`} className={classes.linkIcon}>
                            <Button size="small" color="primary" title="View Patent">
                              View Patent
                            </Button>
                          </Link>
                        </CardActions>
                      </Card>
                    </Grid>
                  ))}
                </Grid>
              </Paper>
            </Grid>
            <Grid item xs={12} md={6}>
              <Paper className={classes.paper}>
                <Typography variant='h5'>In Escrow</Typography>
                <Grid container direction='column'>
                  {escrows && escrows.map(e =>
                    <Grid item key={e.patentId}>
                      <Card className={classes.card}>
                        <CardActionArea>
                          <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                              {e.title}
                            </Typography>
                            <Typography>{`Amount: ${e.escrowBalance}`}</Typography>
                          </CardContent>
                        </CardActionArea>
                        <CardActions>
                          <Link to={`/exchange/${e.patentId}`} className={classes.linkIcon}>
                            <Button size="small" color="primary">
                              View Market
                            </Button>
                          </Link>
                          <Link to={`/patent/${e.patentId}`} className={classes.linkIcon}>
                            <Button size="small" color="primary">
                              View Patent
                            </Button>
                          </Link>
                          <MuiLink href={`https://rinkeby.etherscan.io/address/${e.escrowAddress}`} target="_blank" rel="noreferrer">
                            <Button size="small" color="primary">
                              View Escrow
                            </Button>
                          </MuiLink>
                        </CardActions>
                      </Card>
                    </Grid>
                  )}
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paper}>
            <Typography variant='h5'>Compound tokens</Typography>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>UID</TableCell>
                  <TableCell>Title</TableCell>
                  <TableCell>Balance</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {compounds && compounds.map(c => (
                  <TableRow onClick={() => forwardTo(`exchange/${c.patentId}`)} hover>
                    <TableCell>{c.uid}</TableCell>
                    <TableCell>{c.title}</TableCell>
                    <TableCell>{c.balance}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withStyles(styles)(Wallet);
