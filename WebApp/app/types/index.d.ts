import { Reducer, Store } from 'redux';
import { ContainerState as AppState } from '../containers/App/types';
import { DomainState as AuthenticationState } from '../domain/authentication/types';
import { DomainState as ExchangeDomainState } from '../domain/exchange/types';
import { DomainState as PatentDomainState } from '../domain/patent/types';
import { ContainerState as PatentState } from '../containers/PatentDetails/types';
import { ContainerState as WalletState } from '../containers/WalletPage/types';
import { ContainerState as ExchangePageState } from '../containers/ExchangePage/types';
import { ContainerState as OrganisationState } from '../containers/OrganisationPage/types';
import { ContainerState as PatentListingPageState} from '../containers/PatentListingPage/types';
import { ContainerState as LoginPageState } from '../containers/LoginPage/types';
import { ContainerState as SignupPageState } from '../containers/SignUpPage/types';
import { ContainerState as BuySellCompoundContainerState } from '../containers/BuySellCompoundContainer/types';

export interface LifeStore extends Store<{}> {
  injectedReducers?: any;
  injectedSagas?: any;
  runSaga(saga: () => IterableIterator<any>, args: any): any;
}

export interface InjectReducerParams {
  key: keyof ApplicationRootState;
  reducer: Reducer<any, any>;
}

export interface InjectSagaParams {
  key: keyof ApplicationRootState;
  saga: () => IterableIterator<any>;
  mode?: string | undefined;
}

// Your root reducer type, which is your redux state types also
export interface ApplicationRootState {
  readonly app: AppState;
  readonly authentication: AuthenticationState;
  readonly loginPage: LoginPageState;
  readonly signupPage: SignupPageState;
  readonly patents: PatentState;
  readonly wallet: WalletState;
  readonly exchange: ExchangeDomainState;
  readonly patentsDomain: PatentDomainState;
  readonly exchangePage: ExchangePageState;
  readonly organisation: OrganisationState;
  readonly patentListingPage: PatentListingPageState;
  readonly buySellCompoundContainer: BuySellCompoundContainerState;
}
