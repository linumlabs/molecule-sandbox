/*
 *
 * BuySellCompoundContainer actions
 *
 */

import { createAsyncAction, createStandardAction } from 'typesafe-actions';
import { } from './types';

import ActionTypes from './constants';

export const getBuyCost = createAsyncAction(
    ActionTypes.GET_BUY_COST_REQUEST,
    ActionTypes.GET_BUY_COST_SUCCESS,
    ActionTypes.GET_BUY_COST_FAILURE)
    <{ patentId: string, tokenAmount: number }, {buyCost: number}, string>();

export const getSellReward = createAsyncAction(
    ActionTypes.GET_SELL_REWARD_REQUEST,
    ActionTypes.GET_SELL_REWARD_SUCCESS,
    ActionTypes.GET_SELL_REWARD_FAILURE)
    <{ patentId: string, tokenAmount: number }, {sellReward: number}, string>();

export const reset = createStandardAction(ActionTypes.RESET) <undefined>();
export const cancelRequest = createStandardAction(ActionTypes.CANCEL_REQUEST) <undefined>();

/**
 * Sets the `currentlySending_BuySell` state, which displays a loading indicator during requests
 * @param  {boolean} sending True means we're sending a request, false means we're not
 */
export const setApiSendingFlag = createStandardAction(ActionTypes.SET_API_SENDING_FLAG)<boolean>();

