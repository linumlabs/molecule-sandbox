import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';

/* --- STATE --- */
interface BuySellCompoundContainerState {
  readonly costToBuy: number,
  readonly rewardForSell: number,
  readonly currentlySending: boolean,
  readonly buySellTransaction: boolean,
  readonly buySellTransactionMessage: string | null,
}

/* --- ACTIONS --- */
type BuySellCompoundContainerActions = ActionType<typeof actions>;

/* --- EXPORTS --- */

type RootState = ApplicationRootState;
type ContainerState = BuySellCompoundContainerState;
type ContainerActions = BuySellCompoundContainerActions;

export { RootState, ContainerState, ContainerActions };
