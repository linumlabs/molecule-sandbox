import { put, select, call, takeLatest, debounce, cancel, take, delay, race, fork, cancelled } from 'redux-saga/effects';
import * as WalletActions from '../../domain/wallet/actions';
import { ApplicationRootState } from 'types';
import { buyCostApi, sellRewardApi } from 'api/api';
import * as BuySellActions from './actions';

export function* buyCost(action) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  const { patentId, tokenAmount } = action.payload;

  try {
    yield put(BuySellActions.setApiSendingFlag(true));
    const [response,] = yield race([
      call(buyCostApi, patentId, tokenAmount, apiKey),
      take(BuySellActions.cancelRequest)
    ]);
    if (response) { yield put(BuySellActions.getBuyCost.success(response.data)) }
    yield put(BuySellActions.setApiSendingFlag(false));
  } catch (error) {
    yield put(BuySellActions.getBuyCost.failure(error.message));
    yield put(BuySellActions.setApiSendingFlag(false));
    return false
  } finally {
    yield cancelled();
    yield put(BuySellActions.setApiSendingFlag(false));
  }
}

export function* sellReward(action) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  const { patentId, tokenAmount } = action.payload;

  try {
    yield put(BuySellActions.setApiSendingFlag(true));
    const [response,] = yield race([
      call(sellRewardApi, patentId, tokenAmount, apiKey),
      take(BuySellActions.cancelRequest)
    ]);

    if (response) { yield put(BuySellActions.getSellReward.success(response.data)) }
    yield put(BuySellActions.setApiSendingFlag(false));
  } catch (error) {
    yield put(BuySellActions.getSellReward.failure(error.message));
    yield put(BuySellActions.setApiSendingFlag(false));
    return false
  }
}

export default function* root() {
  yield put(WalletActions.getBalance.request());
  yield debounce(1000, BuySellActions.getBuyCost.request, buyCost);
  yield debounce(1000, BuySellActions.getSellReward.request, sellReward);
}
