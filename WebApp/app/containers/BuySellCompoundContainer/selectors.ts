import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the buySellCompoundContainer state domain
 */

const selectBuySellCompoundContainerDomain = (state: ApplicationRootState) => {
  return state ? state : initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by BuySellCompoundContainer
 */

const selectBuySellCompoundContainer = () =>
  createSelector(selectBuySellCompoundContainerDomain, substate => {
    return substate;
  });

export default selectBuySellCompoundContainer;
export { selectBuySellCompoundContainerDomain };
