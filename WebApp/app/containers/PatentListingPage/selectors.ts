import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';

const selectPatentDomain = (state: ApplicationRootState) => state.patents;

const selectPatentListingPage = () =>
  createSelector(selectPatentDomain, substate => {
    return substate;
  });

export default selectPatentListingPage;
