/**
 *
 * PatentListingPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import PatentListing from '../../components/PatentListing';
import * as PatentListActions from '../../domain/patent/actions';
import reducer from './reducer';
import saga from './saga';

interface OwnProps { }

interface StateProps {
  patents: [{
    id: any,
    name: string,
    type: string,
    createdAt: string,
    status: string,
  }];
}

interface DispatchProps {
  getPatentsData(): void;
}

type Props = StateProps & DispatchProps & OwnProps;

export class PatentListingPage extends React.Component<Props> {
  public componentDidMount() {
    this.props.getPatentsData();
  }

  public render() {
    return (
      <PatentListing patents={this.props.patents} />
    );
  }
}

const mapStateToProps = (state) => ({
  patents: Object.values(state.patents)
});

const mapDispatchToProps = (dispatch: Dispatch, ownProps: OwnProps): DispatchProps => ({
  getPatentsData: () => {
    dispatch(PatentListActions.getPatents.request());
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer<OwnProps>({ key: 'patentListingPage', reducer: reducer });
const withSaga = injectSaga<OwnProps>({ key: 'patentListingPage', saga: saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PatentListingPage);
