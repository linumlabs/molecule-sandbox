/**
 *
 * Organisation
 *
 */

import * as React from 'react';
import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';
import { compose, Dispatch } from 'redux';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import * as Yup from 'yup';
//  import selectOrganisation from './selectors';
import reducer from './reducer';
import saga from './saga';

// import { RootState } from './types';
import CreateOrganisationForm from 'components/CreateOrganisationForm';
import { Formik } from 'formik';
import { createOrgRequest } from './actions';

interface OwnProps {}

interface StateProps {
  currentlySending: boolean;
  error: string;
}

interface DispatchProps {
  onSubmitCreateOrg: (data) => void;
}

type Props = StateProps & DispatchProps & OwnProps;

const CreateOrgFormSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, 'Too Short!')
    .required('Required'),
  registrationNumber: Yup.string()
    .required('Required'),
});

function OrganisationPage(props: Props) {
  const { onSubmitCreateOrg, error, currentlySending } = props;

  return (
    <Formik
      initialValues={{
        name: '',
        registrationNumber: '' }}
      validationSchema={CreateOrgFormSchema}
      onSubmit={(values, actions) => {
        onSubmitCreateOrg(values);
        actions.setSubmitting(currentlySending);
      }}
      render={({submitForm, isSubmitting}) =>(
        <CreateOrganisationForm
          error={error}
          isSubmitting={isSubmitting}
          submitForm={submitForm} />
      )}
    />
  );
}

// const mapStateToProps = createStructuredSelector<RootState, StateProps>({
//   organisation: selectOrganisation(),
// });

function mapDispatchToProps(dispatch: Dispatch, ownProps: OwnProps): DispatchProps {
  return {
    onSubmitCreateOrg: (data) => {
      dispatch(createOrgRequest(data));
    },
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

// <OwnProps> restricts access to the HOC's other props. This component must not do anything with reducer hoc
const withReducer = injectReducer<OwnProps>({ key: 'organisation', reducer: reducer });
// <OwnProps> restricts access to the HOC's other props. This component must not do anything with saga hoc
const withSaga = injectSaga<OwnProps>({ key: 'organisation', saga: saga });

// export default withReducer(withSaga(withConnect(Organisation))); // identical to compose function, but requires no explicit type declaration
export default compose<TReducer, TSaga, TConnect, ReturnType>(
  withReducer,
  withSaga,
  withConnect,
)(OrganisationPage);

type ReturnType = React.ComponentType<OwnProps>;
type TReducer = ReturnType;
type TSaga = ReturnType;
type TConnect = typeof OrganisationPage;
