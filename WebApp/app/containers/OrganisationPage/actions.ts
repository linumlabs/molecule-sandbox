/*
 *
 * Organisation actions
 *
 */

import { } from './types';

import ActionTypes from './constants';

/**
 * Tells the app we want to create an organisation
 * @param  {object} data                    The data we're sending for registration
 * @param  {string} data.name               The name of the organisation to create
 * @param  {string} data.registrationNumber The registration number of the organisation to create
 */
export function createOrgRequest(data) {
    return {type: ActionTypes.CREATE_ORG_REQUEST, data: data};
}
