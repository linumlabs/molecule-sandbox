import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';

/* --- STATE --- */
interface OrganisationState {
  readonly default: any;
}


/* --- ACTIONS --- */
type OrganisationActions = ActionType<typeof actions>;


/* --- EXPORTS --- */

type RootState = ApplicationRootState;
type ContainerState = OrganisationState;
type ContainerActions = OrganisationActions;

export { RootState, ContainerState, ContainerActions };