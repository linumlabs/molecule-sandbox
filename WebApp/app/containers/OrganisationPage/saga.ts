import {call, fork, put, select, take} from 'redux-saga/effects';
import { ApplicationRootState } from 'types';
// import { createOrganisation as createOrgApi  }from '../../api/api';
import { forwardTo } from '../../utils/history';
import ActionTypes from './constants';

export function * createOrg(name, registrationNumber) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    let response;
    // response = yield call(createOrgApi, name, registrationNumber, apiKey);
    return response;
  } catch (error) {
    return false;
  }
}

export function * createOrganisationFlow() {
  while (true) {
    const request = yield take(ActionTypes.CREATE_ORG_REQUEST);
    const {name, registrationNumber} = request.data;
    yield call(createOrg, name, registrationNumber);
    yield call(forwardTo, '/dashboard');
  }
}

export default function * root() {
  yield fork(createOrganisationFlow);
}
