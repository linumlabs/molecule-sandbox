import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the organisation state domain
 */

const selectOrganisationDomain = (state: ApplicationRootState) => {
  return state ? state : initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by Organisation
 */

const selectOrganisation = () =>
  createSelector(selectOrganisationDomain, substate => {
    return substate;
  });

export default selectOrganisation;
export { selectOrganisationDomain };
