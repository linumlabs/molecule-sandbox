import { Dashboard, InsertChartOutlined, AccountBalanceWallet, Cake } from '@material-ui/icons';
import DashboardContainer from 'containers/DashboardPage';
import ExchangePage from 'containers/ExchangePage';
import LandingPage from 'components/LandingPage';
import WalletPage from 'containers/WalletPage';
import PatentListingPage from 'containers/PatentListingPage';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import PatentDetails from 'containers/PatentDetails';
import LoginPage from 'containers/LoginPage';
import SignUpPage from 'containers/SignUpPage';

export interface AppRoute {
  name: string;
  path: string;
  component: React.ComponentType;
  isProtected: boolean;
  isNavRequired: boolean;
  routeNavLinkIcon?: React.ComponentType<SvgIconProps>; // Should be provided if Nav is required
}

const routes: AppRoute[] = [{
  name: 'Landing Page',
  path: '/',
  component: LandingPage,
  isProtected: false,
  isNavRequired: false,
}, {
  name: 'Dashboard',
  path: '/dashboard',
  component: DashboardContainer,
  isProtected: true,
  isNavRequired: true,
  routeNavLinkIcon: Dashboard,
}, {
  name: 'Exchange',
  path: '/exchange',
  component: ExchangePage,
  isProtected: true,
  isNavRequired: true,
  routeNavLinkIcon: InsertChartOutlined,
}, {
  name: 'Exchange',
  path: '/exchange/:id?',
  component: ExchangePage,
  isProtected: true,
  isNavRequired: false,
}, {
  name: 'Wallet',
  path: '/wallet',
  component: WalletPage,
  isProtected: true,
  isNavRequired: true,
  routeNavLinkIcon: AccountBalanceWallet,
}, {
  name: 'Patents',
  path: '/patents',
  component: PatentListingPage,
  isProtected: true,
  isNavRequired: true,
  routeNavLinkIcon: Cake,
}, {
  name: 'Patent',
  path: '/patent/:id?',
  component: PatentDetails,
  isProtected: true,
  isNavRequired: false,
}, {
  name: 'Login',
  path: '/login',
  component: LoginPage,
  isProtected: false,
  isNavRequired: false,
}, {
  name: 'Sign Up',
  path: '/signup',
  component: SignUpPage,
  isProtected: false,
  isNavRequired: false,
}];

export default routes;
