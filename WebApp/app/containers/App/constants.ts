/*
 * These are the variables that determine what our central data store (`../reducers/index.js`)
 * changes in our state.
 */

enum ActionTypes {
  SET_API_SENDING_FLAG = 'molecule/app/SET_API_SENDING_FLAG',
}

export default ActionTypes;
