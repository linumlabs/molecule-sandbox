import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the patentDetails state domain
 */

const selectPatentDetailsDomain = (state: ApplicationRootState) => {
  return state ? state : initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by PatentDetails
 */

const selectPatentDetails = () =>
  createSelector(selectPatentDetailsDomain, substate => {
    return substate;
  });

export default selectPatentDetails;
export { selectPatentDetailsDomain };
