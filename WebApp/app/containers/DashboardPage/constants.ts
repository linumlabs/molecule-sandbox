/*
 *
 * DashboardPage constants
 *
 */

enum ActionTypes {
  DEFAULT_ACTION = 'app/DashboardPage/DEFAULT_ACTION',
}

export default ActionTypes;
