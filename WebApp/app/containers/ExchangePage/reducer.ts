/*
 *
 * ExchangePage reducer
 *
 */

import { getType } from 'typesafe-actions';
import * as ExchangePageActions from './actions';
import { ContainerActions, ContainerState } from './types';

export const initialState: ContainerState = {
  marketHistory: [],
  transactionHistory: [],
};

function exchangePageReducer(state: ContainerState = initialState, action: ContainerActions) {
  switch (action.type) {
    case getType(ExchangePageActions.getMarketHistory.success):
      return  Object.assign({}, state, {
        marketHistory: action.payload,
      });
    case getType(ExchangePageActions.getTransactionHistory.success):
      return  Object.assign({}, state, {
        transactionHistory: action.payload,
      });
    default:
      return state;
  }
}

export default exchangePageReducer;

