import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';
import { TransactionDatapoint } from './dto/TransactionDatapoint';

/* --- STATE --- */
interface ExchangePageState {
  readonly marketHistory: Array<TransactionDatapoint>,
  readonly transactionHistory: Array<TransactionDatapoint>
}

/* --- ACTIONS --- */
type ExchangePageActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = ExchangePageState;
type ContainerActions = ExchangePageActions;

export { RootState, ContainerState, ContainerActions };
