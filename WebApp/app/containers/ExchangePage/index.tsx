/**
 *
 * ExchangePage
 *
 */

import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

import * as ExchangeActions from '../../domain/exchange/actions';
import * as PatentActions from '../../domain/patent/actions';
import * as ExchangePageActions from './actions';
import reducer from './reducer';
import saga from './saga';
import MarketListing from 'components/MarketListing';
import { ApplicationRootState } from 'types';
import TransactionHistory from 'components/TransactionHistory';
import Grid from '@material-ui/core/Grid';
import { TransactionDatapoint } from './dto/TransactionDatapoint';
import { RouteComponentProps } from 'react-router-dom';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import MarketChartLayout from 'components/MarketChartLayout';
import { MarketDetails } from 'domain/exchange/types';
import { forwardTo } from 'utils/history';
import PatentSummary from 'components/PatentSummary';
import { IPatent } from 'domain/patent/types';
import BuySellCompoundContainer from 'containers/BuySellCompoundContainer';

interface RouteParams {
  id?: string; // must be type string since route params
}

interface OwnProps extends RouteComponentProps<RouteParams>, React.Props<RouteParams> { }

interface StateProps {
  exchange: MarketDetails[],
  marketHistory: TransactionDatapoint[],
  transactionHistory: TransactionDatapoint[],
  currentlySending: boolean,
  patent: IPatent,
}

interface DispatchProps {
  getExchangeData(): void;
  getMarketHistory(): void;
  getTransactionHistory(): void;
  getPatents(): void;
}

type Props = StateProps & DispatchProps & OwnProps;

class ExchangePage extends React.Component<Props> {
  state = {
    currentMarket: {
      id: '',
      compoundName: '',
      price: 0,
      totalSupply: 0,
      marketCap: 0,
      balance: 0,
      amountInEscrow: 0,
    },
    currentMarketHistory: [],
    currentTransactionHistory: [],
  };

  public constructor(props) {
    super(props);
  }

  componentDidMount() {
    if(this.props.match.params.id === 'undefined' || !this.props.exchange.find(market => market.id === this.props.match.params.id)) {
      forwardTo(`/exchange`);
    }

    this.props.getExchangeData();
    this.props.getMarketHistory();
    this.props.getTransactionHistory();
    this.props.getPatents();
  }

  componentDidUpdate(prevProps) {
    if(prevProps.match.params.id !== this.props.match.params.id){
      this.props.getMarketHistory();
      this.props.getTransactionHistory();
    }
    if(prevProps.marketHistory !== this.props.marketHistory) {
      this.setState({currentMarketHistory: this.props.marketHistory});
    }
    if(prevProps.transactionHistory !== this.props.transactionHistory) {
      this.setState({currentTransactionHistory: this.props.transactionHistory});
    }
    if(prevProps.exchange !== this.props.exchange) {
      if(this.props.exchange.length <= 0) return;
      if(!this.props.match.params.id) {
        forwardTo(`/exchange/${this.props.exchange[0].id}`);
        return;
      }
      this.setState({currentMarket: this.props.exchange.find(market => market.id === this.props.match.params.id) || this.props.exchange[0]});
    }
  }

  public render() {
    const { 
      exchange,  
      match: { params: { id } }, 
      currentlySending, 
      patent 
    } = this.props;
    const { currentMarket, currentMarketHistory, currentTransactionHistory } = this.state;

    const reduceTags = (previousValue: {id: string, text: string}, currentValue: {id: string, text: string}) => `${previousValue.text}, ${currentValue.text}`;
    
    return (
      <Fragment>
        <Grid container spacing={16}>
          <Grid item xs={7}>
            <MarketChartLayout
              display={!currentlySending}
              marketHistoryProps={{
                currentTokenValue: currentMarket.price,
                currentTokenSupply: currentMarket.totalSupply,
                marketCap: currentMarket.marketCap,
                amountInEscrow: currentMarket.amountInEscrow,
                marketHistory: currentMarketHistory,
              }}
              marketSupplyProps={{
                currentTokenValue: currentMarket.price,
                currentTokenSupply: currentMarket.totalSupply,
                marketCap: currentMarket.marketCap,
                amountInEscrow: currentMarket.amountInEscrow,
              }}
            />
            {patent && 
              <PatentSummary
                id={patent.id} //currentMarket.id
                patentSummary={{
                  uid: patent.uid,
                  title: patent.title,
                  abstract: patent.abstract,
                  drugType: patent.drugType,
                  therapeuticArea: patent.therapeuticAreas.reduce((previousValue, currentValue) => {
                    return {id: '', text: reduceTags(previousValue, currentValue)}
                  }, {id: '', text: ''}).text,
                }}
                marketAddress={patent.marketAddress}/>
            }
            <TransactionHistory display={!currentlySending} transactions={currentTransactionHistory} />
          </Grid>
          <Grid item xs={5}>
            <MarketListing exchange={exchange} currentMarketId={id} />
            {id &&
              <BuySellCompoundContainer patentIdFromExchange={id}/>
            }
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = (state: ApplicationRootState, { match: { params: { id } } }: Props) => ({ 
  exchange: Object.values(state.exchange),
  marketHistory: state.exchangePage.marketHistory,
  transactionHistory: state.exchangePage.transactionHistory,
  currentlySending: state.app.currentlySending,
  patent: id && state.patents && state.patents[id] ?
    state.patents[id] : null,
});

function mapDispatchToProps(dispatch: Dispatch, props: OwnProps): DispatchProps {
  return {
    getExchangeData: () => {
      dispatch(ExchangeActions.getExchangeList.request());
    },
    getMarketHistory: () => {
      if (props.match.params.id && props.match.params.id !== 'undefined') dispatch(ExchangePageActions.getMarketHistory.request({patentId: props.match.params.id}))
    },
    getTransactionHistory: () => {
      if (props.match.params.id && props.match.params.id !== 'undefined') dispatch(ExchangePageActions.getTransactionHistory.request({patentId: props.match.params.id}))
    },
    getPatents: () => {
      if (props.match.params.id && props.match.params.id !== 'undefined') dispatch(PatentActions.getPatents.request())
    }
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

// <OwnProps> restricts access to the HOC's other props. This component must not do anything with reducer hoc
const withReducer = injectReducer<OwnProps>({ key: 'exchangePage', reducer: reducer });
// <OwnProps> restricts access to the HOC's other props. This component must not do anything with saga hoc
const withSaga = injectSaga<OwnProps>({ key: 'exchangePage', saga: saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ExchangePage);
