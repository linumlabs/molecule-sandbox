import { getMarketHistory, getTransactionHistory } from 'api/api';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ApplicationRootState } from 'types';
import * as ExchangeActions from './actions';
import ActionTypes from './constants';

export function* getMarketHistoryRequest(action) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(getMarketHistory, action.payload.patentId, apiKey);
    yield put(ExchangeActions.getMarketHistory.success(response.data));
  } catch (error) {
    yield put(ExchangeActions.getMarketHistory.failure(error));
    return false;
  }
}

export function* getTransactionHistoryRequest(action) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(getTransactionHistory, action.payload.patentId, apiKey);
    yield put(ExchangeActions.getTransactionHistory.success(response.data));
  } catch (error) {
    yield put(ExchangeActions.getTransactionHistory.failure(error));
    return false;
  }
}

export default function* root() {
  yield takeLatest(ActionTypes.GET_MARKET_HISTORY_REQUEST, getMarketHistoryRequest);
  yield takeLatest(ActionTypes.GET_TRANSACTION_HISTORY_REQUEST, getTransactionHistoryRequest);
}