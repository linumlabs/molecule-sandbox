/*
 *
 * ExchangePage actions
 *
 */


import { createAsyncAction } from 'typesafe-actions';
import { } from './types';

import ActionTypes from './constants';

export const getMarketHistory = createAsyncAction(
    ActionTypes.GET_MARKET_HISTORY_REQUEST,
    ActionTypes.GET_MARKET_HISTORY_SUCCESS,
    ActionTypes.GET_MARKET_HISTORY_FAILURE,)
  <{patentId: string}, string, string>();

export const getTransactionHistory = createAsyncAction(
    ActionTypes.GET_TRANSACTION_HISTORY_REQUEST,
    ActionTypes.GET_TRANSACTION_HISTORY_SUCCESS,
    ActionTypes.GET_TRANSACTION_HISTORY_FAILURE,)
  <{patentId: string}, string, string>();