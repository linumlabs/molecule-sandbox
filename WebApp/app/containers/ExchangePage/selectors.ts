import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the exchangePage state domain
 */

const selectExchangePageDomain = (state: ApplicationRootState) => {
  return state ? state : initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by ExchangePage
 */

const selectExchangePage = () =>
  createSelector(selectExchangePageDomain, substate => {
    return substate;
  });

export default selectExchangePage;
export { selectExchangePageDomain };
