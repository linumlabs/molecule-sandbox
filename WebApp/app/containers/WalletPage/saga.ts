import { getWalletBalances } from 'api/api';
import ActionTypes from 'containers/WalletPage/constants';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ApplicationRootState } from 'types';
import * as WalletActions from '../../domain/wallet/actions';

// This file is just a stub showing a sample Api request saga.
// For more information on Saga see: https://redux-saga.js.org/

// Individual exports for testing
export function* getWallet() {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(getWalletBalances, apiKey);
    yield put(WalletActions.getBalance.success(response.data));
  } catch (error) {
    yield put(WalletActions.getBalance.failure(error));
    return false;
  }
}

export default function* root() {
  yield put(WalletActions.getBalance.request());
}
