/*
 *
 * WalletPage constants
 *
 */

enum ActionTypes {
  GET_WALLET_BALANCE_REQUEST = 'molecule/wallet/GET_WALLET_BALANCE_REQUEST',
  GET_WALLET_BALANCE_SUCCESS = 'molecule/wallet/GET_WALLET_BALANCE_SUCCESS',
  GET_WALLET_BALANCE_FAILURE = 'molecule/wallet/GET_WALLET_BALANCE_FAILURE',
}

export default ActionTypes;
