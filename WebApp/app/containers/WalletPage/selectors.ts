import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the walletPage state domain
 */

const selectWalletPageDomain = (state: ApplicationRootState) => {
  return state ? state : initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by WalletPage
 */

const selectWalletPage = () =>
  createSelector(selectWalletPageDomain, substate => {
    return substate;
  });

export default selectWalletPage;
export { selectWalletPageDomain };
