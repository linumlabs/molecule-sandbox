/*
 *
 * WalletPage actions
 *
 */

import { action, createAsyncAction } from 'typesafe-actions';
import { } from './types';

import ActionTypes from './constants';

export const getBalance = createAsyncAction(
    ActionTypes.GET_WALLET_BALANCE_REQUEST,
    ActionTypes.GET_WALLET_BALANCE_SUCCESS,
    ActionTypes.GET_WALLET_BALANCE_FAILURE)
  <void, 
  { 
    compounds: [];
    patents: [];
    nftTokens: [];
    daiBalance: number;
    walletAddress: string;
  }, 
  string>();
