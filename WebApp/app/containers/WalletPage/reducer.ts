/*
 *
 * WalletPage reducer
 *
 */

import { getType } from 'typesafe-actions';
import * as WalletActions from './actions';
import { ContainerActions, ContainerState } from './types';

export const initialState: ContainerState = {
  daiBalance: 0,
  compounds: [],
  nftTokens: [],
  patents: [],
  walletAddress: '0x',
};

function walletPageReducer(state: ContainerState = initialState, action: ContainerActions) {
  switch (action.type) {
    case getType(WalletActions.getBalance.success):
      return {
        ...action.payload,
      };
    default:
      return state;
  }
}

export default walletPageReducer;

