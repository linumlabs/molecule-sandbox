/**
 *
 * CreateNft
 *
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';
import CreateNftForm from 'components/CreateNftForm';
import { createNft } from 'domain/patent/actions';
import { Formik } from 'formik';
import { ApplicationRootState } from 'types';

interface OwnProps {
  patentId: string;
  closeDialog(): void;
}

interface DispatchProps {
  onSubmitCreateNft(data): void;
}

interface StateProps {
  currentlySending: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

const CreateNft: React.SFC<Props> = (props: Props) => {
  const { patentId, onSubmitCreateNft, closeDialog, currentlySending } = props;

  return ( 
    <Fragment>
      <Formik
        initialValues={{
          patentId,
        }}
        isInitialValid={false}
        onSubmit={(values, actions) => {
          onSubmitCreateNft(values);
          actions.setSubmitting(currentlySending);
        }}
        render={({ submitForm }) =>
          <CreateNftForm 
            submitForm={submitForm} 
            closeDialog={closeDialog}  
            isSubmitting={currentlySending}/>
        }
      />
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => {
  return {
    onSubmitCreateNft: (data) => {
      dispatch(createNft.request(data))
    }
  };
}

const mapStateToProps = (state: ApplicationRootState) => ({
  // error: state.loginPage.error,
  currentlySending: state.app.currentlySending,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(CreateNft);
