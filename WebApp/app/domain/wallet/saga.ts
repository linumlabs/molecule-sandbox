import { getWalletBalances } from 'api/api';
import ActionTypes from 'containers/WalletPage/constants';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ApplicationRootState } from 'types';
import * as WalletActions from './actions';


export function* getWallet() {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(getWalletBalances, apiKey);
    yield put(WalletActions.getBalance.success(response.data));
  } catch (error) {
    yield put(WalletActions.getBalance.failure(error));
    return false;
  }
}

export default function* root() {
  yield takeLatest(ActionTypes.GET_WALLET_BALANCE_REQUEST, getWallet);
}
