import { call, fork, put, take, select, delay, cancel } from 'redux-saga/effects';
import { forwardTo } from '../../utils/history';
import jwtDecode from 'jwt-decode';
import { login, signUp, refresh, revoke } from '../../api/api';
import ActionTypes from './constants';
import * as authenticationActions from './actions';
import { ApplicationRootState } from 'types';
import { getType } from 'typesafe-actions';
import { signup } from './actions';

export function* authorize({ email, password }) {
  try {
    let response;
    response = yield call(login, email, password);
    return response;
  } catch (error) {
    yield put(authenticationActions.login.failure(error.message));
  }
}

export function* register(email, password, firstName, lastName) {
  try {
    let response;
    response = yield call(signUp, email, password, firstName, lastName);
    return response;
  } catch (error) {
    yield put(authenticationActions.signup.failure(error.message));
    return false;
  }
}

export function* refreshTokenFlow(refreshToken) {
  try {
    const apiTokens = yield call(refresh, refreshToken);
    yield put(authenticationActions.saveTokens(apiTokens.data));
    return apiTokens;
  } catch (error) {
    if (error.message.includes('Authentication Error')) {
      yield put(authenticationActions.logout());
      yield put(authenticationActions.login.failure('You\'ve been logged out. Please login again.'));
    } else {
      yield put(authenticationActions.login.failure('The server did not respond. Try again.'));
    }
  }
}

export function* loginFlow() {
  while (true) {
    const request = yield take(ActionTypes.AUTH_REQUEST);
    const { email, password } = request.payload;

    const authResult = yield call(authorize, { email: email, password: password });

    if (authResult && authResult.success) {
      yield put(authenticationActions.saveTokens({ accessToken: authResult.data.accessToken, refreshToken: authResult.data.refreshToken }))
      yield call(forwardTo, '/dashboard');
      // Ensure that this action fires last, as otherwise this generator is cancelled before it is able to complete
      // This is a bit of a leaky implementation, and could be improved.
      yield put(authenticationActions.login.success({ userId: authResult.data.userId }));
    }
  }
}

export function* signupFlow() {
  while (true) {
    const request = yield take(signup.request);
    const { email, password, firstName, lastName } = request.payload;
    yield call(register, email, password, firstName, lastName);
    yield put(signup.success());
    yield forwardTo('/login');
  }
}

export function* refreshTokenPoller() {
  while (true) {
    const refreshToken = yield select((state: ApplicationRootState) => state.authentication.refreshToken);
    const accessToken = yield select((state: ApplicationRootState) => state.authentication.accessToken);
    let delayDuration;
    let decodedAccessToken;
    try {
      decodedAccessToken = yield call(jwtDecode, accessToken);
    } catch (error) {
      const newTokens = yield call(refreshTokenFlow, refreshToken);
      decodedAccessToken = yield call(jwtDecode, newTokens.data.accessToken);
    }

    delayDuration = (decodedAccessToken.exp - Date.now() / 1000) * 0.9;
    if ((Date.now() / 1000) + (delayDuration + 1) >= decodedAccessToken.exp) {
      yield call(refreshTokenFlow, refreshToken);
    } else {
      yield delay(delayDuration * 1000);
    }
  }
}

export default function* rootAuthenticationSaga() {
  while (true) {
    const refreshToken = yield select((state: ApplicationRootState) => state.authentication.refreshToken);

    if (!refreshToken) {
      const loginTask = yield fork(loginFlow);
      const signupTask = yield fork(signupFlow);
      yield take(getType(authenticationActions.login.success));
      yield cancel([loginTask, signupTask]);
    } else {
      yield call(refreshTokenFlow, refreshToken);
    }

    const refreshPollerTask = yield fork(refreshTokenPoller);

    yield take(getType(authenticationActions.logout));
    yield call(revoke, refreshToken);
    yield call(forwardTo, '/');
    yield cancel(refreshPollerTask);
  }
}
