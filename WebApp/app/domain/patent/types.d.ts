import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';

interface IPatent {
  id: string,
  type: number,
  title: string,
  uid: string,
  abstract: string,
  inventors: Array<{ id: string, text: string }>,
  currentAssignee: string,
  filingDate: Date,
  anticipatedExpiration: Date,
  drugType: number,
  therapeuticAreas: Array<{ id: string, text: string }>,
  targets: Array<{ id: string, text: string }>,
  patentCertificate: File,
  additionalDocuments: Array<File>,
  relevantVisualization: File,
  cpcCodes: Array<{ id: string, text: string }>,
  marketAddress: string,
}

/* --- STATE --- */
interface PatentDomainState {
  readonly patents: Array<IPatent>
}

/* --- ACTIONS --- */
type PatentDomainActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type DomainState = PatentDomainState;
type DomainActions = PatentDomainActions;

export { RootState, DomainState, DomainActions, IPatent };