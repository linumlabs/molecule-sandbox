import {schema} from 'normalizr';

const patents = new schema.Entity('patents', {}, {idAttribute: 'id'});

export default patents;
