import { normalize } from 'normalizr';
import { call, fork, put, select, take, takeLatest } from 'redux-saga/effects';
import ActionTypes from "./constants";
import { getPatentsApi, updatePatentApi, createNft as createNftApi, createMarket as createMarketApi } from "../../api/api";
import { forwardTo } from '../../utils/history';
import { createPatent as createPatentApi } from '../../api/api';
import * as PatentActions from "./actions";
import patents from "./schema";
import { ApplicationRootState } from 'types';
import { getType } from 'typesafe-actions';
import { IPatent } from './types';

// Individual exports for testing
export function* getPatents() {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(getPatentsApi, apiKey);
    const normalized = normalize(response.data, [patents]);
    yield put(PatentActions.getPatents.success(normalized.entities.patents));
  } catch (error) {
    yield put(PatentActions.getPatents.failure(error));
    return false;
  }
}

export function* createPatent(patentData: IPatent) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(createPatentApi, patentData, apiKey);
    return response;
  } catch (error) {
    yield put(PatentActions.createPatent.failure(error.message));
    return false;
  }
}

export function* updatePatent(patentData: IPatent) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(updatePatentApi, patentData, apiKey);
    return response;
  } catch (error) {
    yield put(PatentActions.updatePatent.failure(error.message));
    return false;
  }
}

export function* createNft(patentId: string) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(createNftApi, patentId, apiKey);
    return response;
  } catch (error) {
    yield put(PatentActions.createNft.failure(error.message));
    return false;
  }
}

export function* createMarket(patentId: string, gradient: number, marketCap: number, initialReserve: number) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(createMarketApi, patentId, gradient, marketCap, initialReserve, apiKey);
    return response;
  } catch (error) {
    yield put(PatentActions.createMarket.failure(error.message));
    return false;
  }
}

export function* createPatentFlow() {
  while (true) {
    const request = yield take(getType(PatentActions.createPatent.request));
    const result = yield call(createPatent, request.payload);
    // TODO: Add the patent to the store, if the result is successful
    yield call(forwardTo, '/patents');
  }
}

export function* createNftFlow() {
  while (true) {
    const request = yield take(PatentActions.createNft.request);

    const { patentId } = request.payload;

    const result = yield call(createNft, patentId);
    yield put(PatentActions.createNft.success(result));
    // TODO: Update patent status in redux store
    yield call(forwardTo, '/patents');
  }
}

export function* createMarketFlow() {
  while (true) {
    const request = yield take(PatentActions.createMarket.request);

    const { patentId, gradient, marketCap, initialReserve } = request.payload;

    const result = yield call(createMarket, patentId, gradient, marketCap, initialReserve);
    // TODO: Update patent status in redux store
    yield call(forwardTo, '/patents');
  }
}

export function* updatePatentFlow() {
  while (true) {
    const request = yield take(ActionTypes.UPDATE_PATENT_REQUEST);
    const result = yield call(updatePatent, request.payload);
    yield call(forwardTo, '/patents');
  }
}

export default function* root() {
  yield takeLatest(ActionTypes.GET_PATENTS_REQUEST, getPatents);
  yield fork(createPatentFlow);
  yield fork(createNftFlow);
  yield fork(createMarketFlow);
  yield fork(updatePatentFlow);
}

