import ActionTypes from "./constants";
import { takeLatest, select, put, call, take, fork } from "redux-saga/effects";
import { ApplicationRootState } from "types";
import { normalize } from 'normalizr';
import { getExchange } from "../../api/api";
import * as ExchangeActions from "./actions";
import * as ExchangePageActions from "../../containers/ExchangePage/actions";
import exchange from "./schema";
import { buyApi, sellApi } from "../../api/api";
import * as WalletActions from '../../domain/wallet/actions';

export function* getExchangeList() {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(getExchange, apiKey);
    const normalized = normalize(response.data, [exchange]);
    yield put(ExchangeActions.getExchangeList.success(normalized.entities.exchange));
  } catch (error) {
    yield put(ExchangeActions.getExchangeList.failure(error));
    return false;
  }
}

export function* buy(patentId: string, tokenAmount: number) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(buyApi, patentId, tokenAmount, apiKey);
    yield put(ExchangeActions.buyCompoundTokens.success(response.data));
    return true;
  } catch (error) {
    yield put(ExchangeActions.buyCompoundTokens.failure(error.message));
    return false;
  } 
}

export function* sell(patentId: string, tokenAmount: number) {
  const apiKey = yield select((state: ApplicationRootState) => state.authentication.accessToken);
  try {
    const response = yield call(sellApi, patentId, tokenAmount, apiKey);
    yield put(ExchangeActions.sellCompoundTokens.success(response.data));
    return true;
  } catch (error) {
    yield put(ExchangeActions.sellCompoundTokens.failure(error.message));
    return false;
  } 
}

export function* buyFlow() {
  while (true) {
    const request = yield take(ExchangeActions.buyCompoundTokens.request);
    const { patentId, tokenAmount } = request.payload;
    const success = yield call(buy, patentId, tokenAmount);
    
    if(success) {
      yield put(ExchangeActions.getExchangeList.request());
      yield put(WalletActions.getBalance.request());
      yield put(ExchangePageActions.getTransactionHistory.request({"patentId": patentId}));
    }
  }
}

export function* sellFlow() {
  while (true) {
    const request = yield take(ActionTypes.SELL_REQUEST);
    const { patentId, tokenAmount } = request.payload;
    const success = yield call(sell, patentId, tokenAmount);

    if(success) {
      yield put(ExchangeActions.getExchangeList.request());
      yield put(WalletActions.getBalance.request());
      yield put(ExchangePageActions.getTransactionHistory.request({"patentId": patentId}));
    }
  }
}

export default function* root() {
  yield takeLatest(ExchangeActions.getExchangeList.request, getExchangeList);
  yield fork(buyFlow);
  yield fork(sellFlow);
}

