import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';

interface MarketDetails {
  readonly id: string,
  readonly compoundName: string,
  readonly price: number,
  readonly totalSupply: number,
  readonly marketCap: number,
  readonly balance: number,
  readonly amountInEscrow: number,
}

/* --- STATE --- */
interface ExchangeDomainState {
  readonly exchange: Array<MarketDetails>
}

/* --- ACTIONS --- */
type ExchangeDomainActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type DomainState = ExchangeDomainState;
type DomainActions = ExchangeDomainActions;

export { RootState, DomainState, DomainActions, MarketDetails };
