import apiRequest from './apiRequest';
import apiUrlBuilder from './apiUrlBuilder';
import { IPatent } from 'domain/patent/types';
import formDataHelper from './formDataHelper';

export function login(email: string, password: string): Promise<any> {
  const body = JSON.stringify({ email: email, password: password });
  return apiRequest('POST', apiUrlBuilder.login, body, 'application/json');
}

export function signUp(email: string, password: string, firstName: string, lastName: string): Promise<any> {
  const body = JSON.stringify({ email: email, password: password, firstName: firstName, lastName: lastName });
  return apiRequest('POST', apiUrlBuilder.signUp, body, 'application/json');
}

export function refresh(refreshToken: string): Promise<any> {
  const body = JSON.stringify({ refreshToken: refreshToken });
  return apiRequest('POST', apiUrlBuilder.refresh, body, 'application/json');
}

export function revoke(refreshToken: string): Promise<any> {
  const body = JSON.stringify({ refreshToken: refreshToken });
  return apiRequest('POST', apiUrlBuilder.revoke, body, 'application/json');
}

// export function getOrganisations(apiToken): Promise<any> {
//   return apiRequest('GET', apiUrlBuilder.,undefined,'application/json', true, apiToken);
// }

// export function createOrganisation(name, registrationNumber, apiToken): Promise<any> {
//   const relativeUri = 'organizations';
//   return apiRequest('GET', relativeUri,undefined,'application/json', true, apiToken);
// }

export async function createPatent(patentData: IPatent, apiToken: string) {
  const requestData = formDataHelper(patentData);

  return apiRequest(
    'POST',
    apiUrlBuilder.patent,
    requestData,
    undefined, // The Content-Type header is set automatically via the FormData object.
    true,
    apiToken);
}

export async function updatePatentApi(patentData: IPatent, apiToken: string) {
  const requestData = formDataHelper(patentData);

  return apiRequest(
    'PUT',
    apiUrlBuilder.updatePatent(patentData.id),
    requestData,
    undefined, // The Content-Type header is set automatically via the FormData object.
    true,
    apiToken);
}

export function getPatentsApi(apiToken: string) {
  return apiRequest('GET', apiUrlBuilder.patentList, undefined, 'application/json', true, apiToken);
}

export function getWalletBalances(apiToken: string) {
  return apiRequest('GET', apiUrlBuilder.walletBalances, undefined, 'application/json', true, apiToken);
}

export async function getPdfAttachment(attachmentId: string) {
  return apiRequest('GET', apiUrlBuilder.attachmentStream(attachmentId), undefined, 'application/json', false);
}

export function getImage(attachmentId: string) {
  return apiRequest('GET', apiUrlBuilder.attachmentBase64(attachmentId), undefined, 'application/json', false);
}

export function getExchange(apiToken: string) {
  return apiRequest('GET', apiUrlBuilder.getExchange, undefined, 'application/json', true, apiToken);
}

export function getTransactionHistory(patentId: string, apiToken: string) {
  return apiRequest('POST', apiUrlBuilder.getTransactionHistory(patentId), undefined, 'application/json', true, apiToken);
}

export function getMarketHistory(patentId: string, apiToken: string) {
  return apiRequest('POST', apiUrlBuilder.getMarketHistory(patentId), undefined, 'application/json', true, apiToken);
}

export function createNft(patentId: string, apiToken: string) {
  const body = JSON.stringify({ patentId: patentId });
  return apiRequest('POST', apiUrlBuilder.createNft(patentId), body, 'application/json', true, apiToken);
}

export function createMarket(patentId: string, gradient: number, marketCap: number, initialReserve: number, apiToken: string) {
  const body = JSON.stringify({ gradient: gradient, marketCap: marketCap, initialReserve: initialReserve });
  return apiRequest('POST', apiUrlBuilder.createMarket(patentId), body, 'application/json', true, apiToken);
}

export function buyCostApi(patentId: string, tokenAmount: number, apiToken: string) {
  return apiRequest('GET', apiUrlBuilder.buyCost(patentId, tokenAmount), undefined, 'application/json', true, apiToken);
}

export function sellRewardApi(patentId: string, tokenAmount: number, apiToken: string) {
  return apiRequest('GET', apiUrlBuilder.sellReward(patentId, tokenAmount), undefined, 'application/json', true, apiToken);
}
 
export function buyApi(patentId: string, tokenAmount: number, apiToken: string) {
  const body = JSON.stringify({tokenAmount});
  return apiRequest('POST', apiUrlBuilder.buy(patentId), body, 'application/json', true, apiToken);
}

export function sellApi(patentId: string, tokenAmount: number, apiToken: string) {
  const body = JSON.stringify({tokenAmount});
  return apiRequest('POST', apiUrlBuilder.sell(patentId), body, 'application/json', true, apiToken);
}