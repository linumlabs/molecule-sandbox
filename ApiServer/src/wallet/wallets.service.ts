import { Inject, Injectable } from '@nestjs/common';
import { ethers } from 'ethers';
import { Modules } from 'src/app.constants';
import { MarketService } from 'src/market/market.service';
import { MarketFactoryService } from 'src/marketfactory/marketfactory.service';
import { NftRegistryService } from 'src/nftregistry/nftregistry.service';
import { PdaiService } from 'src/pdai/pdai.service';
import { User } from 'src/user/user.schema';
import { Logger } from 'winston';
import { PatentService } from '../patent/patent.service';
import GetBalancesResponse from './interface/getBalancesResponse';

@Injectable()
export class WalletsService {
  constructor(@Inject(Modules.Logger) private readonly logger: Logger,
    @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider,
    private readonly patentService: PatentService,
    private readonly pseudoDaiService: PdaiService,
    private readonly marketService: MarketService) { }

  public async createJSONWallet(password: string) {
    const wallet = ethers.Wallet.createRandom(this.ethersProvider);
    return await wallet.encrypt(password);
  }

  public async unlockWallet(walletJson: string, password: string): Promise<ethers.Wallet> {
    const wallet = await ethers.Wallet.fromEncryptedJson(walletJson, password);
    return wallet.connect(this.ethersProvider);
  }

  public async sendEther(wallet: ethers.Wallet, address: string, value: string): Promise<boolean> {
    try {
      const tx = {
        to: address,
        value: ethers.utils.parseEther(value),
      };
      const result = await (await wallet.sendTransaction(tx)).wait();
      this.logger.log('info', `Transaction successfully processed in Block ${result.blockNumber}`);
      return true;
    } catch (error) {
      this.logger.log('error', error.message);
      throw new Error('Something went wrong while allocating test Ether');
    }
  }

  public async deployContract(wallet: ethers.Wallet, abi: string, bytecode: string): Promise<any> {
    if (!wallet.provider) {
      this.logger.log('warn', 'Wallet has no provider. attempting to connect to a provider');
      const provider = this.ethersProvider;
      wallet = wallet.connect(provider);
      this.logger.log('info', 'Connected to provider');
    }

    const factory = new ethers.ContractFactory(abi, bytecode, wallet);
    const contract = await factory.deploy();
    return contract.address;
  }

  public async getBalances(wallet: ethers.Wallet, user: User): Promise<GetBalancesResponse> {
    const pDaiBalance = await this.pseudoDaiService.balanceOf(wallet, wallet.address);

    const patentsInMarket = await this.patentService.getAllInMarketPatents();
    
    const compoundTokens = await Promise.all(patentsInMarket.map(async p => ({
      patentId: p.id.toString(),
      tokenId: p.tokenId,
      marketAddress: p.marketAddress,
      title: p.title,
      balance: await this.marketService.balanceOf(wallet, wallet.address, p.marketAddress),
      escrowBalance: p.initialReserve,
      escrowAddress: p.escrowAddress,
      createdBy: p.createdBy.toString(),
      uid: p.uid,
    })))

    return {
      daiBalance: pDaiBalance,
      walletAddress: await wallet.getAddress(),
      compounds: compoundTokens.filter((c => c.balance > 0)),
      escrows: compoundTokens.filter(c => c.createdBy === user.id),
      nfts: await this.patentService.getNftPatentsForUser(user),
    };
  }
}
