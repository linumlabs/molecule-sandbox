import { Patent } from "src/patent/patent.schema";

interface CompoundTokenInformation {
    patentId: String,
    tokenId: String,
    marketAddress: String,
    title: String,
    balance: Number,
    escrowBalance: String,
    escrowAddress: String,
    createdBy: String,
    uid: String,
}

interface GetBalancesResponse {
    daiBalance: number,
    walletAddress: String,
    compounds: Array<CompoundTokenInformation>,
    escrows: Array<CompoundTokenInformation>,
    nfts: Array<Patent>,
}

export default GetBalancesResponse;