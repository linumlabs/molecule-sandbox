import { Body, Controller, Get, HttpException, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import Express from 'express';
import { User } from 'src/user/user.schema';
import { UsersService } from 'src/user/users.service';
import { WalletsService } from './wallets.service';
import GetBalancesResponse from './interface/getBalancesResponse';

@Controller('wallet')
export class WalletController {
  constructor(private readonly walletService: WalletsService,
              private readonly userService: UsersService) {}

  @Get('balances')
  @UseGuards(AuthGuard('jwt'))
  public async getBalances(@Req() request: Express.Request & {user: User}): Promise<GetBalancesResponse | HttpException> {
    const userWallet = await this.userService.unlockWallet(request.user);
    const result = await this.walletService.getBalances(userWallet, request.user);
    return result;
  }
}
