import { Inject, Injectable } from '@nestjs/common';
import { ethers } from 'ethers';
import { Modules } from 'src/app.constants';
import IMarket from '@molecule-protocol/contracts';
import { MarketFactoryService } from 'src/marketfactory/marketfactory.service';
import { PdaiService } from 'src/pdai/pdai.service';
import { Logger } from 'winston';


@Injectable()
export class MarketService {
  private marketFactoryContract;
  private readonly marketSettings = {
    eventSig: {
      minted: 'Minted(uint256,uint256)',
    },
  };
  constructor(@Inject(Modules.Logger) private readonly logger: Logger,
              @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider,
              private readonly pseudoDaiService: PdaiService) { }

  public async buyCost(wallet: ethers.Wallet, marketAddress: string, tokenVolume: number): Promise<number> {
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const daiRequiredRaw = await connectedContract.priceToMint(ethers.utils.parseUnits(tokenVolume.toString(), 18));
      return Number.parseFloat(ethers.utils.formatUnits(daiRequiredRaw, 18));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async buy(wallet: ethers.Wallet, marketAddress: string, tokenVolume: number): Promise<any> {
    try {
      this.marketFactoryContract = await new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = await this.marketFactoryContract.connect(wallet);
      const pdaiCost = await connectedContract.priceToMint(ethers.utils.parseUnits(tokenVolume.toString(), 18));
      await this.pseudoDaiService.approve(wallet, marketAddress, pdaiCost);
      const mintReceipt = await (await connectedContract.mint(ethers.utils.parseUnits(tokenVolume.toString(), 18))).wait();
      const {from, to, value} = (mintReceipt.events.filter(event => event.eventSignature === 'Transfer(address,address,uint256)'))[0].args;
      const convertedValue = ethers.utils.formatUnits(value, 18);
      
      return {
        convertedValue: convertedValue,
        from: from,
        to: to,
      };
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async rewardForBurn(wallet: ethers.Wallet, marketAddress: string, tokenVolume: number): Promise<number> {
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const reward = await connectedContract.rewardForBurn(ethers.utils.parseUnits(tokenVolume.toString(), 18));
      return Number.parseFloat(ethers.utils.formatUnits(reward, 18)); // Can format on the receiving end as this is a specific number
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async burn(wallet: ethers.Wallet, marketAddress: string, tokenVolume: number): Promise<any> {
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const mintReceipt = await (await connectedContract.burn(ethers.utils.parseUnits(tokenVolume.toString(), 18))).wait();
      const {from, to, value} = (mintReceipt.events.filter(event => event.eventSignature === 'Transfer(address,address,uint256)'))[0].args;
      const convertedValue = ethers.utils.formatUnits(value, 18);
      return {
        convertedValue: convertedValue,
        from: from,
        to: to,
      };
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async getMinted(wallet: ethers.Wallet, marketAddress: string, mintedAddress: string): Promise<any> {
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const filteredNFTs = connectedContract.filters.Minted(null, 0); // uint256 amount, uint256 totalCost)
      filteredNFTs.fromBlock = 0;
      const events = await this.ethersProvider.getLogs(filteredNFTs);
      return events.map(e => connectedContract.interface.parseLog(e));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async totalSupply(wallet: ethers.Wallet, marketAddress: string): Promise<number> {
    // Current amount of tokens minted
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const total = await connectedContract.totalSupply();
      return Number.parseFloat(ethers.utils.formatUnits(total, 18));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async marketCap(wallet: ethers.Wallet, marketAddress: string): Promise<number> {
    // Total amount that can be minted
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const marketCap = await connectedContract.marketCap();
      return Number.parseFloat(ethers.utils.formatUnits(marketCap, 18));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async nftTokenID(wallet: ethers.Wallet, marketAddress: string): Promise<number> {
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const tokenID = await connectedContract.nftTokenID();
      return tokenID;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async balanceOf(wallet: ethers.Wallet, who: string, marketAddress: string): Promise<number> {
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const balance = await connectedContract.balanceOf(who);
      return Number.parseFloat(ethers.utils.formatUnits(balance, 18));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async transfer(wallet: ethers.Wallet, addressTo: string, value: number, marketAddress: string): Promise<boolean> {
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const success = await (await connectedContract.transfer(
        addressTo,
        ethers.utils.parseUnits(value.toString(), 18),
      )).wait();
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async getEscrow(wallet: ethers.Wallet, marketAddress: string): Promise<any> {
    try {
      this.marketFactoryContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const escrowAddress = await connectedContract.getEscrow();
      return escrowAddress;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }
}
