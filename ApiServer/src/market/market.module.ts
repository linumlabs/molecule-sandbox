import { Module } from '@nestjs/common';
import { PdaiModule } from 'src/pdai/pdai.module';
import { MarketService } from './market.service';

@Module({
  imports: [PdaiModule],
  providers: [MarketService],
  exports: [MarketService],
})

export class MarketModule {}
