import { forwardRef, HttpException, Inject, Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ethers } from 'ethers';
import { Model } from 'mongoose';
import { Modules, Schemas } from 'src/app.constants';
import { AttachmentsService } from 'src/attachments/attachments.service';
import { UserDocument, User } from 'src/user/user.schema';
import { EscrowFactoryService } from '../escrowfactory/escrowfactory.service';
import { MarketFactoryService } from '../marketfactory/marketfactory.service';
import { NftRegistryService } from '../nftregistry/nftregistry.service';
import { UsersService } from '../user/users.service';
import { CreateMarketDTO } from './dto/createMarket.dto';
import { CreatePatentDTO } from './dto/createPatent.dto';
import { UpdatePatentDTO } from './dto/updatePatent.dto';
import { PatentDocument, PatentStatus, Patent } from './patent.schema';

@Injectable()
export class PatentService {
  constructor(@InjectModel(Schemas.Patent) private readonly patentRepository: Model<PatentDocument>,
    @Inject(Modules.Logger) private readonly logger: Logger,
    @Inject(forwardRef(() => UsersService))
    private readonly userService: UsersService,
    private readonly attachmentService: AttachmentsService,
    private readonly nftRegistryService: NftRegistryService,
    private readonly escrowFactoryService: EscrowFactoryService,
    private readonly marketFactoryService: MarketFactoryService) { }

  public async create(user: UserDocument,
    createPatentDto: CreatePatentDTO,
    files): Promise<Patent | HttpException> {
    const createdPatent = new this.patentRepository({
      ...createPatentDto,
      createdBy: user.id,
    });

    await Promise.all([].concat(...Object.values(files))
      .map(async file => {
        const attachment = await this.attachmentService.create({
          filename: file.originalname,
          contentType: file.mimetype,
        }, file)
        if (Array.isArray(createdPatent[file.fieldname])) {
          createdPatent[file.fieldname].push(attachment)
        } else {
          createdPatent[file.fieldname] = attachment
        }
      })
    );

    await createdPatent.save();
    return createdPatent.toObject();
  }

  public async update(patentId: string,
    user: UserDocument,
    patent: UpdatePatentDTO,
    files: any)
    : Promise<Patent | HttpException> {
    const patentToUpdate = await this.patentRepository.findById(patentId);
    if (patentToUpdate.createdBy.toString() !== user.id) throw new UnauthorizedException('You do not own this patent');

    await patentToUpdate.update(patent);
    await Promise.all([].concat(...Object.values(files))
      .map(async file => {
        const attachment = await this.attachmentService.create({
          filename: file.originalname,
          contentType: file.mimetype,
        }, file)
        if (Array.isArray(patentToUpdate[file.fieldname])) {
          patentToUpdate[file.fieldname].push(attachment)
        } else {
          patentToUpdate[file.fieldname] = attachment
        }
      })
    );

    await patentToUpdate.save();
    return patentToUpdate.toObject();
  }

  async createNFT(user: User, patentId: string): Promise<Patent | HttpException> {
    const patent = await this.patentRepository.findById(patentId);
    if (patent.createdBy.toString() !== user.id) { throw new UnauthorizedException('You do not own this patent'); }
    try {
      const wallet = await this.userService.unlockWallet(user);
      const tokenId = await this.nftRegistryService.publishMolecule(wallet, patent.title);

      patent.tokenId = tokenId;
      patent.status = PatentStatus.Nft;
      await patent.save();
      return patent.toObject();
    } catch (e) {
      this.logger.log('error', e);
    }
  }

  public async createMarket(user: User,
    createMarketDTO: CreateMarketDTO,
    patentId: string)
    : Promise<Patent | HttpException> {
    const patent = await this.patentRepository.findById(patentId);
    if (patent.createdBy.toString() !== user.id) throw new UnauthorizedException('You do not own this patent');

    try {
      const initialReserveAsTokens = createMarketDTO.marketCap * (createMarketDTO.initialReserve / 100);
      const wallet = await this.userService.unlockWallet(user);
      const escrowAddress = await this.escrowFactoryService.deployEscrow(
        wallet,
        0,
        wallet.address,
      );
      const marketAddress = await this.marketFactoryService.deployMarket(
        wallet,
        0, // TODO: user needs to select market type
        patent.tokenId,
        ethers.utils.parseUnits(initialReserveAsTokens.toString(), 18).toString(),
        ethers.utils.parseUnits(createMarketDTO.marketCap.toString(), 18).toString(),
        escrowAddress,
        createMarketDTO.gradient.toString(),
      );
      await this.nftRegistryService.safeTransferFrom(
        wallet,
        wallet.address,
        marketAddress,
        patent.tokenId,
      );
      patent.marketAddress = marketAddress;
      patent.marketCap = createMarketDTO.marketCap.toString();
      patent.initialReserve = initialReserveAsTokens.toString();
      patent.gradient = createMarketDTO.gradient.toString();
      patent.escrowAddress = escrowAddress;
      patent.status = PatentStatus.InMarket;
      await patent.save();
      return patent.toObject();
    } catch (e) {
      this.logger.log('error', e);
    }
  }

  public async getPatent(id: string): Promise<Patent> {
    const patent = await this.patentRepository.findById(id);
    return patent.toObject();
  }

  public async list(user: UserDocument): Promise<Array<Patent> | HttpException> {
    const patents = await this.patentRepository.find().or([
      { status: PatentStatus.Nft },
      { status: PatentStatus.InMarket },
      { status: PatentStatus.Created, createdBy: user.id }
    ]).populate('createdBy')
      //.populate('additionalDocuments')
      .sort({ createdAt: +1 });
    return patents.map(p => p.toObject());
  }

  public async getNftPatentsForUser(user: User): Promise<Patent[]> {
    const patents = await this.patentRepository.find({ status: PatentStatus.Nft, createdBy: user.id })
    return patents.map(p => p.toObject());
  }

  public async patentsInMarketByUser(user: User): Promise<Patent[]> {
    const patents = await this.patentRepository.find({ status: PatentStatus.InMarket, createdBy: user.id });
    return patents.map(p => p.toObject());
  }

  public async getAllInMarketPatents(): Promise<Patent[]> {
    const patents = await this.patentRepository.find({ status: PatentStatus.InMarket });
    return patents.map(p => p.toObject());
  }

  public async getPatentByTokenId(tokenId: string): Promise<Patent | false> {
    const patent = await this.patentRepository.findOne({ tokenId: tokenId });
    return patent && patent.toObject();
  }

  public async getPatentIdByTokenId(tokenId: string): Promise<string | false> {
    const patent = await this.patentRepository.findOne({ tokenId: tokenId }).select('id');
    return patent ? patent.id : false;
  }
}
