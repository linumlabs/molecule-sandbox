import { ObjectId } from 'bson';
import { Document, Schema, Types } from 'mongoose';
import { Schemas } from 'src/app.constants';
import { User } from '../user/user.schema';
import { AttachmentDocument } from 'src/attachments/attachment.schema';

export enum PatentStatus {
  Created = 'CREATED',
  Nft = 'NFT',
  InMarket = 'IN_MARKET',
  Blacklisted = 'BLACKLISTED',
}

export enum PatentType {
  ProductClaim = 1,
  ProductByProcessClaim,
  ProcessClaim,
  MethodOfUseClaim,
  FormulationClaim,
}

export enum DrugType {
  SmallMolecule = 1,
  Biotech,
}

interface IPatent {
  type: PatentType;
  title: string;
  uid: string;
  abstract: string;
  inventors: Array<{id: string, text: string}>;
  currentAssignee: string;
  filingDate: Date;
  anticipatedExpiration: Date;
  drugType: DrugType;
  therapeuticAreas: Array<{id: string, text: string}>;
  targets: Array<{id: string, text: string}>;
  patentCertificate: AttachmentDocument | ObjectId | string;
  additionalDocuments: Array<AttachmentDocument | ObjectId | string>;
  relevantVisualization: AttachmentDocument | ObjectId | string;
  cpcCodes: Array<{id: string, text: string}>;
  createdBy: User | ObjectId;
  status: PatentStatus;
  // TODO: Move this to separate schema for NFT, Market, etc
  tokenId: string;
  marketAddress: string;
  escrowAddress: string;
  marketCap: string;
  initialReserve: string;
  gradient: string;
}

export interface Patent extends IPatent {
  id: ObjectId | string;
}

export interface PatentDocument extends IPatent, Document { }

const TagSchema = new Schema({
    id: {type: String, required: true},
    text: {type: String, required: true},
  }, {
    id: false,
    _id: false,
});

export const patentSchema = new Schema({
  type: { type: Number, required: true, integer: true },
  title: { type: String, required: true },
  uid:  { type: String, required: true },
  abstract: { type: String, required: true },
  inventors: [TagSchema],
  currentAssignee: { type: String, required: true },
  filingDate: { type: Date, required: true},
  anticipatedExpiration: { type: Date, required: true},
  drugType: { type: Number, required: true },
  therapeuticAreas: [TagSchema],
  targets: [TagSchema],
  patentCertificate: {type: Schema.Types.ObjectId, ref: Schemas.Attachment, required: true},
  additionalDocuments: [{ type: Schema.Types.ObjectId, ref: Schemas.Attachment }],
  relevantVisualization: { type: Schema.Types.ObjectId, ref: Schemas.Attachment, required: true },
  cpcCodes:  [TagSchema],
  createdBy: { type: Schema.Types.ObjectId, ref: Schemas.User },
  status: { type: String, required: true, default: PatentStatus.Created},
  tokenId: { type: String, required: false},
  marketAddress: { type: String, required: false},
  marketCap: { type: String, required: false},
  initialReserve: { type: String, required: false},
  escrowAddress: { type: String, required: false},
  gradient: { type: String, required: false },
  }, {
    timestamps: true,
    toJSON: {
      getters: true,
      versionKey: false,
      transform: (doc, ret): Patent => {
        ret.id = String(ret._id);
        delete ret._id;
        return ret;
      },
      virtuals: true,
    },
    toObject: {
      getters: true,
      versionKey: false,
      transform: (doc, ret): Patent => {
        ret.id = String(ret._id);
        delete ret._id;
        return ret;
      },
    },
  });
