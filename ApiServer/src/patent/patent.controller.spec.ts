import 'jest';
import { Test, TestingModule } from '@nestjs/testing';
import { PatentController } from './patent.controller';

describe('Patent Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [PatentController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: PatentController = module.get<PatentController>(PatentController);
    expect(controller).toBeDefined();
  });
});
