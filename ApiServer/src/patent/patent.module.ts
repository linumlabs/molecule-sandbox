import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Schemas } from 'src/app.constants';
import { AttachmentsModule } from 'src/attachments/attachments.module';
import { UsersModule } from 'src/user/users.module';
import { EscrowFactoryModule } from '../escrowfactory/escrowfactory.module';
import { MarketFactoryModule } from '../marketfactory/marketfactory.module';
import { NftRegistryModule } from '../nftregistry/nftregistry.module';
import { PatentController } from './patent.controller';
import { patentSchema } from './patent.schema';
import { PatentService } from './patent.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: Schemas.Patent, schema: patentSchema }]),
            forwardRef(() => UsersModule),
            AttachmentsModule,
            NftRegistryModule,
            EscrowFactoryModule,
            MarketFactoryModule],
  controllers: [PatentController],
  providers: [PatentService],
  exports: [PatentService],
})
export class PatentModule {}
