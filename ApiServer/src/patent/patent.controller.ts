import {
  Body, Controller, Get, HttpException,
  Param, Post, Put, Req,
  UploadedFiles, UseGuards, UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import Express from 'express';
import { CreateMarketDTO } from './dto/createMarket.dto';
import { CreatePatentDTO } from './dto/createPatent.dto';
import { UpdatePatentDTO } from './dto/updatePatent.dto';
import { Patent } from './patent.schema';
import { PatentService } from './patent.service';
import { FileFieldsInterceptorHelper, FileOptions } from 'src/helper/fileInterceptorHelper';

@Controller('patent')
export class PatentController {
  constructor(private readonly patentService: PatentService) { }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FileFieldsInterceptorHelper([{
    name: 'patentCertificate',
    maxCount: 1,
    type: FileOptions.PDF,
  }, {
    name: 'additionalDocuments',
    maxCount: 5,
    type: FileOptions.PDF,
  }, {
    name: 'relevantVisualization',
    maxCount: 1,
    type: FileOptions.PICTURE,
  }]))
  public async create(@Body() patent: CreatePatentDTO,
    @Req() request: Express.Request & { user: any },
    @UploadedFiles() files):
    Promise<Patent | HttpException> {
    return this.patentService.create(request.user, patent, files);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FileFieldsInterceptorHelper([{
    name: 'patentCertificate',
    maxCount: 1,
    type: FileOptions.PDF,
  }, {
    name: 'additionalDocuments',
    maxCount: 5,
    type: FileOptions.PDF,
  }, {
    name: 'relevantVisualization',
    maxCount: 1,
    type: FileOptions.PICTURE,
  }]))
  public async update(@Param('id') patentId,
    @Body() patent: UpdatePatentDTO,
    @Req() request: Express.Request & { user: any },
    @UploadedFiles() files):
    Promise<Patent | HttpException> {
    return this.patentService.update(patentId, request.user, patent, files);
  }

  @Post(':id/createNft')
  @UseGuards(AuthGuard('jwt'))
  public async createNft(@Req() request: Express.Request & { user: any },
    @Param('id') patentId)
    : Promise<Patent | HttpException> {
    const result = this.patentService.createNFT(request.user, patentId);
    return result;
  }

  @Post(':id/createMarket')
  @UseGuards(AuthGuard('jwt'))
  public async createMarket(@Body() marketInformation: CreateMarketDTO,
    @Req() request: Express.Request & { user: any },
    @Param('id') patentId)
    : Promise<Patent | HttpException> {
    const result = this.patentService.createMarket(request.user, marketInformation, patentId);
    return result;
  }

  @Get('list')
  @UseGuards(AuthGuard('jwt'))
  public async getPatentList(@Req() request: Express.Request & { user: any }): Promise<any | HttpException> {
    const patentList = await this.patentService.list(request.user);
    return patentList;
  }

  @Get('listPatents')
  @UseGuards(AuthGuard('jwt'))
  public async getListPatent(@Req() request: Express.Request & { user: any }): Promise<any | HttpException> {
    const patentList = await this.patentService.patentsInMarketByUser(request.user);
    return patentList;
  }
}
