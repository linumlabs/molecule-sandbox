import { IsInt, Min, Max } from 'class-validator';

export class CreateMarketDTO {
  // un-scaled
  @IsInt()
  @Min(1)
  @Max(Number.MAX_SAFE_INTEGER)
  public gradient: number;
  
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  public initialReserve: number;

  @IsInt()
  @Min(1)
  @Max(Number.MAX_SAFE_INTEGER)
  public marketCap: number;
}
