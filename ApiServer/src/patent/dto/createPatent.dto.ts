import { IsString, IsArray, IsOptional, IsInt, IsDate } from 'class-validator';

export class CreatePatentDTO {
  @IsInt()
  public type: number;
  
  @IsString()
  public title: string;

  @IsString()
  public uid: string;

  @IsString()
  public abstract: string;

  @IsArray()
  public inventors: Array<{id: string, name: string}>
  
  @IsString()
  public currentAssignee: string;

  @IsDate()
  public filingDate: Date
  
  @IsDate()
  public anticipatedExpiration: Date

  @IsInt()
  public drugType: number

  @IsOptional()
  @IsArray()
  public therapeuticAreas: Array<{id: string, name: string}>
  
  @IsOptional()
  @IsArray()  
  public targets: Array<{id: string, name: string}>
  
  @IsArray()
  public cpcCodes: Array<{id: string, name: string}>
}
