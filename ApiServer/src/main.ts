import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as compression from 'compression';
import * as helmet from 'helmet';
import 'reflect-metadata';
import { ApplicationModule } from './app.module';
import { ConfigService } from './config/config.service';

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule);
  const configService = app.get(ConfigService);
  app.use(helmet());
  app.enableCors();
  
  const appConfig = configService.get('app');

  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    transform: true,
    // The below option should be enabled when upgrading class-transformer
    // to version 0.2.3
    // transformOptions: {
    //   implicitTypeConversion: true,
    // }
  }));
  app.use(compression());
  app.setGlobalPrefix(appConfig.routePrefix);

  await app.listen(appConfig.port);
}

bootstrap();
