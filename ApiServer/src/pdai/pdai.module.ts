import { Module } from '@nestjs/common';
import { PdaiService } from './pdai.service';

@Module({
  providers: [PdaiService],
  exports: [PdaiService],
})

export class PdaiModule {}
