import { Inject, Injectable } from '@nestjs/common';
import { ethers } from 'ethers';
import { Modules } from 'src/app.constants';
import { PseudoDaiToken } from '@molecule-protocol/contracts';
import { Logger } from 'winston';
import { ConfigService } from '../config/config.service';

@Injectable()
export class PdaiService {
  private readonly pdaiContract;

  constructor(@Inject(Modules.Logger) private readonly logger: Logger,
              @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider,
              private readonly config: ConfigService) {
    const allContracts = this.config.get('contracts');
    this.pdaiContract = new ethers.Contract(allContracts.pdai, PseudoDaiToken.abi, this.ethersProvider);
  }

  public async fetchRewardState(wallet: ethers.Wallet, userAddress: string): Promise<boolean> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const rewardState = await connectedContract.fetchRewardState(
        userAddress,
      );
      this.logger.log('info', rewardState);
      return rewardState;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async mint(wallet: ethers.Wallet): Promise<void> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const tx = await (await connectedContract.mint()).wait();
      this.logger.log('info', 'Successfully minted pDAI');
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async transfer(wallet: ethers.Wallet, toAddress: string, value: number): Promise<boolean> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const success = await (await connectedContract.transfer(
        toAddress,
        ethers.utils.parseUnits(value.toString(), 18),
      )).wait();
      this.logger.log('info', success);
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async transferFrom(wallet: ethers.Wallet, fromAddress: string, toAddress: string, value: number): Promise<boolean> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const success = await (await connectedContract.transferFrom(
        fromAddress,
        toAddress,
        ethers.utils.parseUnits(value.toString(), 18),
      )).wait();
      this.logger.log('info', success);
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async approve(wallet: ethers.Wallet, spenderAddress: string, amount: number): Promise<boolean> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const success = await (await connectedContract.approve(
        spenderAddress,
        ethers.utils.parseUnits(amount.toString(), 18),
      )).wait();
      this.logger.log('info', success);
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async increaseApproval(wallet: ethers.Wallet, spenderAddress: string, addedAmount: number): Promise<boolean> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const success = await (await connectedContract.increaseApproval(
        spenderAddress,
        ethers.utils.parseUnits(addedAmount.toString(), 18),
      )).wait();
      this.logger.log('info', success);
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async decreaseApproval(wallet: ethers.Wallet, spenderAddress: string, subtractedAmount: number): Promise<boolean> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const success = await (await connectedContract.increaseApproval(
        spenderAddress,
        ethers.utils.parseUnits(subtractedAmount.toString(), 18),
      )).wait();
      this.logger.log('info', success);
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async allowance(wallet: ethers.Wallet, ownerAddress: string, spenderAddress: string): Promise<number> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const allowanceOf = await connectedContract.allowance(
        ownerAddress,
        spenderAddress,
      );
      this.logger.log('info', allowanceOf);
      return Number.parseFloat(ethers.utils.formatUnits(allowanceOf, 18));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async totalSupply(wallet: ethers.Wallet): Promise<number> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const total = await connectedContract.totalSupply();
      this.logger.log('info', total);
      return Number.parseFloat(ethers.utils.formatUnits(total, 18));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async balanceOf(wallet: ethers.Wallet, address: string): Promise<number> {
    try {
      const connectedContract = this.pdaiContract.connect(wallet);
      const balanceOf = await connectedContract.balanceOf(address);

      return Number.parseFloat(ethers.utils.formatUnits(balanceOf, 18));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

}
