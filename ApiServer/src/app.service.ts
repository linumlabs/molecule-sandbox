import { Inject, Injectable } from '@nestjs/common';
import { Logger } from 'winston';
import { Modules } from './app.constants';
import { ConfigService } from './config/config.service';

@Injectable()
export class AppService {
  constructor(private readonly config: ConfigService,
              @Inject(Modules.Logger) private readonly logger: Logger) {}

  public root(): any {
    this.logger.log('info', 'Request to root received');
    return {
      name: this.config.get('app').name,
      version: this.config.get('app').version,
      description: this.config.get('app').description,
    };
  }
}
