import { Injectable, UnauthorizedException, Inject } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from 'src/config/config.service';
import { UsersService } from '../user/users.service';
import { Logger } from 'winston';
import { Modules, Schemas } from 'src/app.constants';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthDocument } from './auth.schema';
import { User } from 'src/user/user.schema';

export interface JwtPayload {
  userId: string;
  // TODO: Add Role or permissions here
}

export interface SignInDto {
  email: string;
  password: string;
}

export enum LoginStatus {
  success = 'SUCCESS',
}

export interface LoginResponse {
  accessToken: string;
  refreshToken: string;
  userId: string;
  status: LoginStatus;
}

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(Schemas.Authentication) private readonly authTokenRepository: Model<AuthDocument>,
    @Inject(Modules.Logger) private readonly logger: Logger,
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) { }

  public async signIn({ email, password }: SignInDto): Promise<LoginResponse> {
    const user = await this.usersService.findByEmail(email);
    if (!user) throw new UnauthorizedException('Invalid Username or Password');

    if (await user.comparePassword(password)) {
      const accessToken = this.generateAccessToken(user.id);
      const refreshToken = this.generateRefreshToken(user.id);
      const refreshTokenDoc = new this.authTokenRepository({ value: refreshToken });
      await refreshTokenDoc.save();

      return {
        accessToken: accessToken,
        refreshToken: refreshToken,
        userId: user.id,
        status: LoginStatus.success,
      };
    } else throw new UnauthorizedException('Invalid Username or Password');
  }

  public async validateUser(payload: JwtPayload): Promise<User | false> {
    return await this.usersService.findById(payload.userId);
  }

  public async refreshToken(tokenId: string, refreshToken: string): Promise<{ accessToken: string, refreshToken: string }> {
    const tokenFromMongo = await this.authTokenRepository.findOne({value: refreshToken});
    if (!tokenFromMongo) throw new UnauthorizedException('Invalid token. Please log-in again.');

    try {
      const decodedToken = this.jwtService.verify(refreshToken);
      const accessToken = this.generateAccessToken(decodedToken.userId);
      this.revokeToken(tokenFromMongo.value);
      const newRefreshToken = this.generateRefreshToken(decodedToken.userId);
      const refreshTokenDoc = new this.authTokenRepository({ value: newRefreshToken });
      await refreshTokenDoc.save();
      return {
        refreshToken: newRefreshToken,
        accessToken: accessToken,
      };
    } catch (error) {
      this.logger.log('error', 'Unauthorized access attempt');
      throw new UnauthorizedException('Invalid token. Please log-in again.');
    }
  }

  public async revokeToken(refreshToken: string): Promise<any> {
    await this.authTokenRepository.findOneAndDelete({value: refreshToken});
  }

  private generateAccessToken(userId: string): string {
    return this.jwtService.sign(
      {
        userId: userId,
        // TODO: Roles, or similar
      },
      {
        expiresIn: `${this.configService.get('jwt').expiry}h`,
        notBefore: '0',
        subject: '', // TODO: see below
        issuer: this.configService.get('app').host,
      },
    );
  }

  private generateRefreshToken(userId: string): string {
    return this.jwtService.sign(
      {
        userId: userId,
      },
      {
        notBefore: '0',
        subject: '', // TODO: populate subject
        issuer: this.configService.get('app').host,
      },
    );
  }
}
