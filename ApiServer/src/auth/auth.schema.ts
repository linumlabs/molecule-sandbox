import { Document, Schema } from 'mongoose';

export interface AuthToken {
  value: string
}

export interface AuthDocument extends AuthToken, Document { }

export const authSchema = new Schema({
  value: { type: String, required: true, index: true },
  }, {
    timestamps: true,
    toJSON: {
      getters: true,
      versionKey: false,
      transform: (doc, ret) => {
        ret.id = String(ret._id);
        delete ret._id;
        return ret;
      },
      virtuals: true,
    },
    toObject: {
      getters: true,
      versionKey: false,
      transform: (doc, ret) => {
        ret.id = String(ret._id);
        delete ret._id;
        return ret;
      },
    },
  });