import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { AuthService, LoginResponse, SignInDto } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @HttpCode(200)
  public async signIn(@Body() signInDto: SignInDto): Promise<LoginResponse> {
    return this.authService.signIn(signInDto);
  }

  @Post('refresh')
  @HttpCode(200)
  public async refreshToken(@Body() data: {accessToken: string, refreshToken: string}): Promise<{accessToken: string, refreshToken: string}> {
    return this.authService.refreshToken('', data.refreshToken);
  }
  
  @Post('revoke')
  @HttpCode(200)
  public async revokeToken(@Body() data: {refreshToken: string}): Promise<any> {
    return this.authService.revokeToken(data.refreshToken);
  }
}
