import * as bcrypt from 'bcrypt';
import { Document, Schema } from 'mongoose';
import { Schemas } from 'src/app.constants';
import { env } from 'src/config/env';
import { Organization } from '../organization/organization.schema';

interface IUser {
  firstName: string;
  lastName: string;
  fullName: string;
  email: string;
  password: string;
  activate: boolean;
  wallet: string;
  organization: Organization | Schema.Types.ObjectId;
  comparePassword(candidatePassword: string): Promise<boolean>;
}

export interface User extends IUser {
  id: string,
}

export interface UserDocument extends IUser, Document { }

export const userSchema = new Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true, select: false },
  wallet: { type: String, select: false },
  organization: { type: Schema.Types.ObjectId, ref: Schemas.Organization },
}, {
  timestamps: true,
  toJSON: {
    getters: true,
    versionKey: false,
    transform: (doc, ret) => {
      ret.id = String(ret._id);
      delete ret._id;
      delete ret.wallet;
      return ret;
    },
    virtuals: true,
    
  },
  toObject: {
    getters: true,
    versionKey: false,
    transform: (doc, ret) => {
      ret.id = String(ret._id);
      delete ret._id;
      delete ret.wallet;
      return ret;
    },
  },
});

userSchema.pre('save', async function (this: UserDocument, next) {
  if (!this.isModified('password')) return next();

  const hashedPassword = await bcrypt.hash(this.password, env.bcrypt.saltRounds);
  this.password = hashedPassword;

  next();
});

userSchema.methods.comparePassword = async function (candidatePassword: string) {
  return await bcrypt.compare(candidatePassword, this.password);
};

userSchema.virtual('fullName').get(function () {
  return this.firstName + ' ' + this.lastName;
});
