import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Schemas } from '../app.constants';
import { PdaiModule } from '../pdai/pdai.module';
import { UsersController } from './users.controller';
import { WalletsModule } from '../wallet/wallet.module';
import { userSchema } from './user.schema';
import { UsersService } from './users.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: Schemas.User, schema: userSchema }]),
            forwardRef(() => WalletsModule),
            PdaiModule],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})

export class UsersModule { }
