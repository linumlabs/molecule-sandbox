import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import Express from 'express';
import { CreateUserDTO } from './dto/create-user.dto';
import { User } from './user.schema';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Post()
  public async create(@Body() createUserDto: CreateUserDTO) {
    await this.userService.create(createUserDto);
    return {result: 'created'};
  }

  @Post('unlockWallet')
  @UseGuards(AuthGuard('jwt'))
  public async unlockWallet(@Req() request: Express.Request & {user: User}) {
    return this.userService.unlockWallet(request.user);
  }
}
