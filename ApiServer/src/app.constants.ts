export enum Schemas {
  User = 'User',
  Organization = 'Organization',
  Patent = 'Patent',
  Attachment = 'Attachment',
  Authentication = 'Authentication',
}

export enum Modules {
  Logger = 'winston',
  EthersProvider = 'EthersProvider',
}
