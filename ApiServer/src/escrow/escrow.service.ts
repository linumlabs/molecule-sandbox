import { Inject, Injectable } from '@nestjs/common';
import { ethers } from 'ethers';
import { Modules } from 'src/app.constants';
import { IMarketFactory } from '@molecule-protocol/contracts';
import { Logger } from 'winston';

@Injectable()
export class EscrowService {
  private escrowContract;
  constructor(@Inject(Modules.Logger) private readonly logger: Logger,
              @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider) { }

  public async withdraw(wallet: ethers.Wallet, marketAddress: string): Promise<boolean> {
    try {
      this.escrowContract = new ethers.Contract(marketAddress, IMarketFactory.abi, this.ethersProvider);
      const connectedContract = this.escrowContract.connect(wallet);
      const success = await (await connectedContract.withdraw()).wait();
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }
}
