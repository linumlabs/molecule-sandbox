import { BadRequestException, HttpException, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Modules, Schemas } from 'src/app.constants';
import { User, UserDocument } from 'src/user/user.schema';
import { Logger } from 'winston';
import { CreateOrganizationDTO } from './dto/create-organization.dto';
import { OrganizationDocument } from './organization.schema';

@Injectable()
export class OrganizationsService {
  constructor(@InjectModel(Schemas.Organization) private readonly organizationRepository: Model<OrganizationDocument>,
              @InjectModel(Schemas.User) private readonly userRepository: Model<UserDocument>,
              @Inject(Modules.Logger) private readonly logger: Logger) {}

  public async create(createOrganizationDto: CreateOrganizationDTO, owner: UserDocument): Promise<OrganizationDocument | HttpException> {
    if (owner.organization !== undefined) return new BadRequestException('A user can only be a part of one organisation');
    const createdOrganization = new this.organizationRepository(createOrganizationDto);
    owner.organization = createdOrganization;
    owner.save();
    return await createdOrganization.save();
  }

  public async findById(organizationId: string): Promise<OrganizationDocument> {
    return await this.organizationRepository.findById(organizationId);
  }

  public async getAll(): Promise<OrganizationDocument[]> {
    const organizations = await this.organizationRepository.find()
      .populate('owner')
      .populate('members');

    return organizations;
  }

  public async addMember(organisationId: string, memberId: string, userId: string)
                        : Promise<OrganizationDocument | HttpException> {
    const organization = await this.organizationRepository.findById(organisationId).populate('owner');
    const owner = await this.userRepository.findById(userId);
    if (organization.owner.id !== owner.id) return new UnauthorizedException('You can only add members to your organisation');

    const member = await this.userRepository.findById(memberId);
    if (member.organization) return new BadRequestException('User already belongs to an organisation');

    member.organization = organization;
    member.save();

    organization.addMember(member);
    return organization.save();
  }

  public async removeMember(organisationId: string, memberId: string, userId: string)
                            : Promise<OrganizationDocument | HttpException> {
    const organization = await this.organizationRepository.findById(organisationId).populate('owner');
    const owner = await this.userRepository.findById(userId);
    if (organization.owner.id !== owner.id) return new UnauthorizedException('You can only add members to your organisation');

    const member = await this.userRepository.findById(memberId);

    member.organization = undefined;
    member.save();

    organization.removeMember(member);
    return organization.save();
  }
}
