import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Schemas } from '../app.constants';
import { PdaiModule } from '../pdai/pdai.module';
import { userSchema } from '../user/user.schema';
import { organizationSchema } from './organization.schema';
import { OrganizationsController } from './organizations.controller';
import { OrganizationsService } from './organizations.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Schemas.Organization, schema: organizationSchema }]),
    MongooseModule.forFeature([{ name: Schemas.User, schema: userSchema }]),
    PdaiModule,
  ],
  controllers: [OrganizationsController],
  providers: [OrganizationsService],
})

export class OrganizationsModule {}
