import { IsMongoId, IsString } from 'class-validator';

export class CreateOrganizationDTO {
  @IsString()
  public name: string;

  @IsString()
  public registrationNumber: string;
}
