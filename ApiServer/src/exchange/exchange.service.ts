import { ethers } from 'ethers';
import { Injectable, Inject, HttpException, NotFoundException } from '@nestjs/common';
import { Logger } from 'winston';
import { Modules } from 'src/app.constants';
import { MarketFactoryService } from 'src/marketfactory/marketfactory.service';
import { MarketService } from 'src/market/market.service';
import { IMarket } from '@molecule-protocol/contracts';
import { NftRegistryService } from 'src/nftregistry/nftregistry.service';
import { ConfigService } from '../config/config.service';
import { TransactionDataPointDTO } from './dto/TransactionDataPoint.dto';
import { MarketDetailsDTO } from './dto/MarketDetails.dto';
import { EventDataDTO } from './dto/EventData.dto';
import { PatentService } from 'src/patent/patent.service';

@Injectable()
export class ExchangeService {
  private marketContract;

  constructor(@Inject(Modules.Logger) private readonly logger: Logger,
    @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider,
    private readonly patentService: PatentService,
    private readonly marketFactoryService: MarketFactoryService,
    private readonly marketService: MarketService,
    private readonly nftRegistryService: NftRegistryService,
    private readonly config: ConfigService) { }

  async getMarketsDetails(wallet: ethers.Wallet): Promise<MarketDetailsDTO[] | NotFoundException> {
    // Get markets data
    const markets = await this.getMarkets(wallet);

    // Map markets data to return object
    const marketsWithPatentId = markets ? await Promise.all(markets.map(
      async (value) => {
        const patentId = await this.patentService.getPatentIdByTokenId(value.tokenId.toString());
        return {
          ...value,
          patentId,
        }
      }
    )) : [];

    const marketsFiltered = marketsWithPatentId.filter((value) => value.patentId);

    const marketDetails: MarketDetailsDTO[] = await Promise.all(marketsFiltered.map(async m => {
      const escrowAddress = await this.marketService.getEscrow(wallet, m.market);
      const patentId = await this.patentService.getPatentIdByTokenId(m.tokenId.toString());

      if (!patentId) throw NotFoundException;

      const result = await {
        id: patentId,
        compoundName: await this.nftRegistryService.tokenURI(wallet, m.tokenId),
        price: await this.marketService.buyCost(wallet, m.market, 1),
        totalSupply: await this.marketService.totalSupply(wallet, m.market),
        marketCap: await this.marketService.marketCap(wallet, m.market),
        balance: await this.marketService.balanceOf(wallet, wallet.address, m.market),
        amountInEscrow: await this.marketService.balanceOf(wallet, escrowAddress, m.market),
      };
      return result;
    }));

    // Filter out any array objects with numbers bigger than Number.MAX_SAFE_INTEGER & failed conditions
    let getValues = (object) => Object.keys(object).map((key) => object[key]);
    let filterMaxSafeInteger = (value: MarketDetailsDTO) => getValues(value)
      .filter((entry) => typeof entry === 'number')
      .every((entry) => entry < Number.MAX_SAFE_INTEGER);
    let filterConditions = (value: MarketDetailsDTO) => {
      return value.totalSupply <= value.marketCap &&
        value.amountInEscrow <= value.totalSupply &&
        (value.amountInEscrow / value.marketCap) <= 1.0
    }

    return marketDetails.filter(filterMaxSafeInteger).filter(filterConditions);
  }

  public async getMarkets(wallet: ethers.Wallet): Promise<any[]> {
    this.logger.log('info', 'Attempting to get markets');
    // Get markets data from events
    const events = await this.marketFactoryService.marketsDeployed(wallet);
    return events && events.map(e => ({ market: e.values._market, tokenId: e.values._tokenId }));
  }

  public async getMarket(wallet: ethers.Wallet, marketAddress: string): Promise<any> {
    this.logger.log('info', 'Attempting to get market');
    return (await this.getMarkets(wallet)).filter(m => m.market === marketAddress);
  }

  public async getTransactionHistory(wallet: ethers.Wallet, patentId: string): Promise<TransactionDataPointDTO[]> {
    const patent = await this.patentService.getPatent(patentId);
    const marketAddress = patent ? patent.marketAddress : "";

    const mintEvents = await this.getMintEvents(wallet, marketAddress);
    const burnEvents = await this.getBurnEvents(wallet, marketAddress);

    const mintData: TransactionDataPointDTO[] = mintEvents && await Promise.all<TransactionDataPointDTO>(mintEvents.map(async e => {
      // Get timestamp of transaction block
      const timestamp = (await this.ethersProvider.getBlock(e.blockNumber)).timestamp;

      // Use blockHash for grouping transactions
      const scaledTokenAmount = Number.parseFloat(ethers.utils.formatUnits(e.event.values.amount, 18));
      const scaledDaiAmount = Number.parseFloat(ethers.utils.formatUnits(e.event.values.totalCost, 18));

      return {
        timestamp: timestamp.toString(),
        blockNumber: e.blockNumber,
        transactionHash: e.transactionHash,
        tokenAmount: scaledTokenAmount,
        daiAmount: -scaledDaiAmount,
        firstTokenPrice: this.getFirstTokenPrice(scaledTokenAmount, scaledDaiAmount),
      } as TransactionDataPointDTO;
    }));

    const burnData: TransactionDataPointDTO[] = burnEvents && await Promise.all<TransactionDataPointDTO>(burnEvents.map(async e => {
      // Get timestamp of transaction block
      const timestamp = (await this.ethersProvider.getBlock(e.blockNumber)).timestamp;

      // Use blockHash for grouping transactions
      const scaledTokenAmount = Number.parseFloat(ethers.utils.formatUnits(e.event.values.amount, 18));
      const scaledDaiAmount = Number.parseFloat(ethers.utils.formatUnits(e.event.values.reward, 18));

      return {
        timestamp: timestamp.toString(),
        blockNumber: e.blockNumber,
        transactionHash: e.transactionHash,
        tokenAmount: scaledTokenAmount,
        daiAmount: scaledDaiAmount,
        firstTokenPrice: this.getFirstTokenPrice(scaledTokenAmount, scaledDaiAmount),
      } as TransactionDataPointDTO;
    }));

    return mintData && burnData ? mintData.concat(burnData).sort((a, b) => (a.timestamp > b.timestamp) ? 1 : -1) : [];
  }

  public async getMarketHistoryDataPoints(wallet: ethers.Wallet, patentId: string): Promise<TransactionDataPointDTO[]> {
    const transactionHistory: TransactionDataPointDTO[] = await this.getTransactionHistory(wallet, patentId);

    // Create reducer to combine datapoints by blockNumber
    const reducer = (accumulator: TransactionDataPointDTO, currentValue: TransactionDataPointDTO) => ({
      timestamp: currentValue.timestamp,
      blockNumber: currentValue.blockNumber,
      transactionHash: null,
      tokenAmount: accumulator.tokenAmount + currentValue.tokenAmount,
      daiAmount: accumulator.daiAmount + currentValue.daiAmount,
      firstTokenPrice: null,
    });

    // Create a unique array of blockNumbers
    const blockNumbers = [...new Set(transactionHistory.map((value) => value.blockNumber))];

    // Reduce TransactionHistory, collecting by blockNumbers
    return blockNumbers.map((blockNumber) => {
      let accumalatedValue = transactionHistory
        .filter((value) => value.blockNumber === blockNumber)
        .reduce(reducer);
      accumalatedValue.firstTokenPrice = this.getFirstTokenPrice(accumalatedValue.tokenAmount, accumalatedValue.daiAmount);
      return accumalatedValue;
    });
  }

  private getFirstTokenPrice(tokenAmount, daiAmount): number {
    // Currently the bonding curve gradient is constant
    const gradient = 1 / 2000;
    // Derivation from definite integral and known parameters
    const totalSupplyBefore = Math.abs(daiAmount) / (gradient * tokenAmount) - 0.5 * tokenAmount;
    // If the Dai amount is zero, this is the escrow amount, so return price of very first token
    return daiAmount == 0 ? 0.5 * gradient : Math.abs(gradient * totalSupplyBefore);
  }

  private async getMintEvents(wallet: ethers.Wallet, marketAddress: string): Promise<EventDataDTO[]> {
    try {
      this.marketContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketContract.connect(wallet);
      const filter = connectedContract.filters.Minted(null, null);
      filter.fromBlock = 0;
      const events = await this.ethersProvider.getLogs(filter);
      return events.map(e => ({ blockNumber: e.blockNumber, transactionHash: e.transactionHash, event: connectedContract.interface.parseLog(e) }));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  private async getBurnEvents(wallet: ethers.Wallet, marketAddress: string): Promise<EventDataDTO[]> {
    try {
      this.marketContract = new ethers.Contract(marketAddress, IMarket.abi, this.ethersProvider);
      const connectedContract = this.marketContract.connect(wallet);
      const filter = connectedContract.filters.Burned(null, null);
      filter.fromBlock = 0;
      const events = await this.ethersProvider.getLogs(filter);
      return events.map(e => ({ blockNumber: e.blockNumber, transactionHash: e.transactionHash, event: connectedContract.interface.parseLog(e) }));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async buyCostCompoundTokens(wallet, patentId: string, tokenAmount: number) {
    const patent = await this.patentService.getPatent(patentId);
    return await this.marketService.buyCost(wallet, patent.marketAddress, tokenAmount);
  }

  public async buyCompoundTokens(wallet, patentId: string, tokenAmount: number) {
    const patent = await this.patentService.getPatent(patentId);
    return await this.marketService.buy(wallet, patent.marketAddress, tokenAmount);
  }

  public async sellRewardCompoundTokens(wallet, patentId: string, tokenAmount: number) {
    const patent = await this.patentService.getPatent(patentId);
    return await this.marketService.rewardForBurn(wallet, patent.marketAddress, tokenAmount);
  }

  public async sellCompoundTokens(wallet, patentId: string, tokenAmount: number) {
    const patent = await this.patentService.getPatent(patentId);
    return await this.marketService.burn(wallet, patent.marketAddress, tokenAmount);
  }
}
