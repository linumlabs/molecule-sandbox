import { IsString, IsNumber } from 'class-validator';

export class TransactionDataPointDTO {
  @IsString()
  public timestamp: string;

  @IsNumber()
  public blockNumber: number;

  @IsString()
  public transactionHash: string;

  @IsNumber()
  public tokenAmount: number;

  @IsNumber()
  public daiAmount: number;

  @IsNumber()
  public firstTokenPrice: number;
}