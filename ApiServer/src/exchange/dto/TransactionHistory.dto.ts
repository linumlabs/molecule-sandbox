import { IsString } from 'class-validator';

export class TransactionHistoryDTO {
  @IsString()
  public marketAddress: string;
}