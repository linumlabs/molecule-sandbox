import { Inject, Injectable } from '@nestjs/common';
import { ethers, Wallet } from 'ethers';
import { Modules } from 'src/app.constants';
import { NFTRegistry } from '@molecule-protocol/contracts';
import { EthersProviderModule } from 'src/ethers/ethersProvider.module';
import { Logger } from 'winston';
import { ConfigService } from '../config/config.service';

@Injectable()
export class NftRegistryService {
  private readonly nftregistryContract;
  private readonly nFTRegistrySettings = {
    name: 'Molecule Patent Registry',
    symbol: 'MPR',
    eventSig: {
      transfer: 'Transfer(address,address,uint256)',
      publishedMolecule: 'PublishedMolecule(uint256,address)',
    },
  };

  constructor(@Inject(Modules.Logger) private readonly logger: Logger,
              @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider,
              private readonly config: ConfigService) {
    const allContracts = this.config.get('contracts');
    this.nftregistryContract = new ethers.Contract(allContracts.nftregistry, NFTRegistry.abi, this.ethersProvider);
  }

  public async name(wallet: ethers.Wallet): Promise<string> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const nameOf = await connectedContract.name();
      this.logger.log('info', nameOf);
      return nameOf;
    } catch (error) {
      this.logger.log('error', error.message);
      throw error;
    }
  }

  public async symbol(wallet: ethers.Wallet): Promise<string> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const symbolOf = await connectedContract.symbol();
      this.logger.log('info', symbolOf);
      return symbolOf;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async getOwner(wallet: ethers.Wallet): Promise<string> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const ownerOf = await connectedContract.getOwner();
      this.logger.log('info', ownerOf);
      return ownerOf;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async tokenURI(wallet: ethers.Wallet, tokenID: number): Promise<string> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const tokenURIOf = await connectedContract.tokenURI(
        tokenID,
      );
      this.logger.log('info', tokenURIOf);
      return tokenURIOf;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async balanceOf(wallet: ethers.Wallet, ownerAddress: string): Promise<number> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const balanceOf = await connectedContract.balanceOf(
        ownerAddress,
      );
      this.logger.log('info', balanceOf);
      return balanceOf;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async ownerOf(wallet: ethers.Wallet, tokenID: string): Promise<string> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const owner = await connectedContract.ownerOf(
        tokenID,
      );
      this.logger.log('info', owner);
      return owner;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async approve(wallet: ethers.Wallet, spenderAddress: string, amount: number): Promise<boolean> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const success = await connectedContract.approve(
        spenderAddress,
        amount,
      );
      this.logger.log('info', success);
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async getApproved(wallet: ethers.Wallet, tokenID: number): Promise<string> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const approvedFor = await connectedContract.getApproved(
        tokenID,
      );
      this.logger.log('info', approvedFor);
      return approvedFor;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async setApprovalForAll(wallet: ethers.Wallet, addressTo: string, approved: boolean): Promise<void> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      await (await connectedContract.setApprovalForAll(
        addressTo,
        approved,
      )).wait();
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async isApprovedForAll(wallet: ethers.Wallet, ownerAddress: string, operatorAddress: string): Promise<boolean> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const approvedForAll = await connectedContract.getApproved(
        ownerAddress,
        operatorAddress,
      );
      this.logger.log('info', approvedForAll);
      return approvedForAll;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async safeTransferFrom(wallet: ethers.Wallet, fromAddress: string, toAddress: string, tokenID: string): Promise<void> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const result = await (await connectedContract.safeTransferFrom(
        fromAddress,
        toAddress,
        tokenID,
      )).wait();
      this.logger.log('info', 'Successfully transferred NFT to Bonded Market');
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async supportsInterface(wallet: ethers.Wallet, interfaceID: string): Promise<boolean> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const supported = await connectedContract.supportsInterface(
        interfaceID,
      );
      this.logger.log('info', supported);
      return supported;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async publishMolecule(wallet: ethers.Wallet, moleculeName: string): Promise<any> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const receipt = await (await connectedContract.publishMolecule(moleculeName)).wait();
      const tokenId = (receipt.events.filter(event => event.eventSignature === this.nFTRegistrySettings.eventSig.transfer))[0].args.tokenId;
      return tokenId;
    } catch (error) {
      this.logger.log('error', error.message);
      throw error;
    }
  }

  public async getTransfers(wallet: ethers.Wallet, fromAddress?: string, toAddress?: string): Promise<any> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const filteredNFTs = connectedContract.filters.Transfer(fromAddress, toAddress, null);
      filteredNFTs.fromBlock = 0;
      const events = await this.ethersProvider.getLogs(filteredNFTs);
      return events.map(e => connectedContract.interface.parseLog(e));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async getMoleculeName(wallet: ethers.Wallet, tokenId: string): Promise<any> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const filter = connectedContract.filters.PublishedMolecule(ethers.utils.bigNumberify(tokenId), null);
      filter.fromBlock = 0;
      const events = await this.ethersProvider.getLogs(filter);
      const parsedEvent = connectedContract.interface.parseLog(events[0]);
      return parsedEvent.values.name;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async blacklist(wallet: ethers.Wallet, tokenID: number): Promise<void> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      await (await connectedContract.blacklist(tokenID)).wait();
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async isBlacklisted(wallet: ethers.Wallet, tokenID: number): Promise<boolean> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const blacklisted = await connectedContract.blacklist(tokenID);
      this.logger.log('info', blacklisted);
      return blacklisted;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async transferFrom(wallet: ethers.Wallet, fromAddress: string, toAddress: string, tokenID: number): Promise<void> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      await (await connectedContract.blacklist(
        fromAddress,
        toAddress,
        tokenID,
      )).wait();
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async publishedBlockNumber(wallet: ethers.Wallet): Promise<number> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const blockNumber = await connectedContract.publishedBlockNumber();
      this.logger.log('info', blockNumber);
      return blockNumber;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async tokenOfOwnerByIndex(wallet: ethers.Wallet, ownerAddress: string, index: number): Promise<number> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const ownedTokens = await connectedContract.tokenOfOwnerByIndex(
        ownerAddress,
        index,
      );
      this.logger.log('info', ownedTokens);
      return ownedTokens;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async totalSupply(wallet: ethers.Wallet): Promise<number> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const total = await connectedContract.totalSupply();
      this.logger.log('info', total);
      return total;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async tokenByIndex(wallet: ethers.Wallet, index: number): Promise<number> {
    try {
      const connectedContract = this.nftregistryContract.connect(wallet);
      const allTokens = await connectedContract.tokenByIndex(
        index,
      );
      this.logger.log('info', allTokens);
      return allTokens;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }
}
