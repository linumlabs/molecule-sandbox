import { Module } from '@nestjs/common';
import { NftRegistryService } from './nftregistry.service';

@Module({
  providers: [NftRegistryService],
  exports: [NftRegistryService],
})

export class NftRegistryModule {}
