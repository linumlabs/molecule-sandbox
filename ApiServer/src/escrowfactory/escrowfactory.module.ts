import { Module } from '@nestjs/common';
import { EscrowFactoryService } from './escrowfactory.service';

@Module({
  providers: [EscrowFactoryService],
  exports: [EscrowFactoryService],
})

export class EscrowFactoryModule {}
