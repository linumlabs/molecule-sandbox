import { Inject, Injectable } from '@nestjs/common';
import { ethers } from 'ethers';
import { Modules } from 'src/app.constants';
import { EscrowFactory } from '@molecule-protocol/contracts';
import { Logger } from 'winston';
import { ConfigService } from '../config/config.service';

@Injectable()
export class EscrowFactoryService {
  private readonly escrowFactoryContract;
  private readonly escrowFactorySettings = {
    eventSig : {
      escrowDeployed: 'EscrowDeployed(address,address,address)',
    },
  };

  constructor(@Inject(Modules.Logger) private readonly logger: Logger,
              @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider,
              private readonly config: ConfigService) {
    const allContracts = this.config.get('contracts');
    this.escrowFactoryContract = new ethers.Contract(allContracts.escrowFactory, EscrowFactory.abi, this.ethersProvider);
  }

  public async registerNewFactory(wallet: ethers.Wallet, factoryAddress: string): Promise<void> {
    try {
      const connectedContract = this.escrowFactoryContract.connect(wallet);
      await (await connectedContract.registerNewFactory(
        factoryAddress,
      )).wait();
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async findFactory(wallet: ethers.Wallet, index: number): Promise<number> {
    try {
      const connectedContract = this.escrowFactoryContract.connect(wallet);
      const address = await connectedContract.findFactory(index);
      this.logger.log('info', address);
      return address;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async deployEscrow(wallet: ethers.Wallet, escrowType: number, nftOwner: string): Promise<any> {
    try {
      const connectedContract = this.escrowFactoryContract.connect(wallet);
      const escrowReceipt = await (await connectedContract.deployEscrow(escrowType, nftOwner)).wait();
      const escrowAddress = (escrowReceipt.events.filter(event =>
          event.eventSignature === this.escrowFactorySettings.eventSig.escrowDeployed))[0].args._newEscrow;
      return ethers.utils.getAddress(escrowAddress);
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async getEscrows(wallet: ethers.Wallet, escrowType: number): Promise<string[]> {
    try {
      const connectedContract = this.escrowFactoryContract.connect(wallet);
      const success = await connectedContract.getEscrows(
        escrowType,
      );
      this.logger.log('info', success);
      return success;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async publishBlock(wallet: ethers.Wallet): Promise<number> {
    try {
      const connectedContract = this.escrowFactoryContract.connect(wallet);
      const block = await connectedContract.publishBlock();
      this.logger.log('info', block);
      return block;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }
}
