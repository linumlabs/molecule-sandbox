import { forwardRef, Module, OnModuleInit } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WinstonModule } from 'nest-winston';
import { format, transports } from 'winston';
import * as mongodbUri from 'mongodb-uri';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { EthersProviderModule } from './ethers/ethersProvider.module';
import { UsersModule } from './user/users.module';
import { AttachmentsModule } from './attachments/attachments.module';
import { OrganizationsModule } from './organization/organization.module';
import { WalletsModule } from './wallet/wallet.module';
import { ExchangeModule } from './exchange/exchange.module';

@Module({
  imports: [ConfigModule,
            EthersProviderModule,
            WinstonModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        transports: [
          new transports.Console({
            level: configService.get('log').level,
            handleExceptions: false,
            format: format.combine(format.prettyPrint(), format.cli()),
          }),
          new transports.File({
            filename: 'error.log',
            level: 'error',
            format: format.combine(format.json()),
          }),
        ],
      }),
      inject: [ConfigService],
    }),
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigService) => ({
        uri: mongodbUri.formatMongoose(configService.get('mongodb')),
        useCreateIndex: true,
        useNewUrlParser: true,
        bufferCommands: false,
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    AuthModule,
    OrganizationsModule,
    WalletsModule,
    AttachmentsModule,
    ExchangeModule,
  ],
  controllers: [AppController],
  providers: [ConfigService, AppService],
})

export class ApplicationModule { }
