import { Inject, Injectable } from '@nestjs/common';
import { ethers, Wallet } from 'ethers';
import { Modules } from 'src/app.constants';
import { MarketFactory } from '@molecule-protocol/contracts';
import { Logger } from 'winston';
import { ConfigService } from '../config/config.service';

@Injectable()
export class MarketFactoryService {
  private readonly marketFactoryContract;
  private readonly marketFactorySettings = {
    eventSig: {
      marketDeployed: 'MarketDeployed(address,address,uint256,uint256)',
    },
  };

  constructor(@Inject(Modules.Logger) private readonly logger: Logger,
              @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider,
              private readonly config: ConfigService) {
    const allContracts = this.config.get('contracts');
    this.marketFactoryContract = new ethers.Contract(allContracts.marketFactory, MarketFactory.abi, this.ethersProvider);
  }

  public async registerNewFactory(wallet: ethers.Wallet, newFactory: string): Promise<void> {
    try {
      const connectedContract = this.marketFactoryContract.connect(wallet);
      await (await connectedContract.registerNewFactory(newFactory)).wait();
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async findFactory(wallet: ethers.Wallet, index: number): Promise<string> {
    try {
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const factoryAddress = await connectedContract.findFactory(index);
      this.logger.log('info', factoryAddress);
      return factoryAddress;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async deployMarket(
    wallet: ethers.Wallet,
    marketType: number,
    tokenID: string,
    initialReserve: string,
    marketCap: string,
    escrowAddress: string,
    gradient: string,
  ): Promise<any> {
    try {
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const marketReceipt = await (await connectedContract.deployMarket(
        marketType,
        tokenID,
        ethers.utils.bigNumberify(initialReserve),
        ethers.utils.bigNumberify(marketCap),
        escrowAddress,
        gradient)).wait();
      const marketAddress = (marketReceipt.events.filter(event =>
          event.eventSignature === this.marketFactorySettings.eventSig.marketDeployed))[0].args._market;
      return ethers.utils.getAddress(marketAddress);
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async getFactoryIndex(wallet: ethers.Wallet, factoryAddress: string): Promise<number> {
    try {
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const index = await connectedContract.getFactoryIndex(
        factoryAddress,
      );
      this.logger.log('info', index);
      return index;
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async marketsDeployed(wallet: ethers.Wallet): Promise<any> {
    try {
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const publishBlockNumber = 0; // await this.publishBlock(wallet);
      const filterMarketsDeployed = connectedContract.filters.MarketDeployed(null, null, null);
      filterMarketsDeployed.fromBlock = publishBlockNumber;
      const events = await this.ethersProvider.getLogs(filterMarketsDeployed);
      return events.map(e => connectedContract.interface.parseLog(e));
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }

  public async publishBlock(wallet: ethers.Wallet): Promise<number> {
    try {
      const connectedContract = this.marketFactoryContract.connect(wallet);
      const blockNumber = await connectedContract.publishBlock();
      this.logger.log('info', blockNumber.toString()); // TODO: confirm to string

      return Number.parseInt(blockNumber.toString(), 0);
    } catch (error) {
      this.logger.log('error', error.message);
    }
  }
}
