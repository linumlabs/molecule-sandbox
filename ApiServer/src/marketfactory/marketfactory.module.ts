import { Module } from '@nestjs/common';
import { MarketFactoryService } from './marketfactory.service';

@Module({
  providers: [MarketFactoryService],
  exports: [MarketFactoryService],
})

export class MarketFactoryModule {}
